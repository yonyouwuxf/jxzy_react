import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { actions } from "mirrorx";

import Header from 'components/Header';
import MonitorSampleTable from '../monitorSample-table';
import MonitorSampleForm from '../monitorSample-form';

import './index.less';

/**
 * MonitorSampleRoot Component
 */
class MonitorSampleRoot  extends Component {
    constructor(props) {
        super(props);
        this.state = { }
    }
    /**
     *
     */
    componentWillMount() {
        this.getTableData();
    }
    /**
     * 获取table表格数据
     */
    getTableData = () => {
        actions.monitorSample.loadList();
    }

    render() {
        let { pageSize, pageIndex, totalPages} = this.props;
        return (
            <div className='monitorSample-root'>
                <Header title='监控抽样' back={true}/>
                <MonitorSampleForm { ...this.props }/>
                <MonitorSampleTable { ...this.props }/>
            </div>
        )
    }
}
export default MonitorSampleRoot;