import React, { Component } from 'react'
import { actions } from "mirrorx";
import { Switch, InputNumber, Col, Row,FormControl, Label, Select,Radio,Form } from "tinper-bee";
import SearchPanel from 'components/SearchPanel';
import DatePicker from "tinper-bee/lib/Datepicker";
// import RefMultipleTableWithInput from 'pap-refer/lib/ref-multiple-table/src/index';
// import 'pap-refer/lib/ref-multiple-table/src/index.css';
import {RefMultipleTableWithInput} from 'pap-refer/dist/index';
 import 'pap-refer/dist/index.css';
import { deepClone } from "utils";
import moment from 'moment';
const { RangePicker } = DatePicker;
const FormItem = Form.FormItem;
const format = "YYYY-MM-DD";
import './index.less'

class MonitorSampleForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            pk_monitorSample: '',
        }
    }
    componentWillMount(){
        // 获得监控抽样列表数据
        actions.monitorSample.getOrderTypes();
    }
    /** 查询数据
     * @param {*} error 校验是否成功
     * @param {*} values 表单数据
     */
    search = (error,values) => {
        this.props.form.validateFields(async (err, values) => {
            if(values.billDate[0]) {
                values.startBillDate = values.billDate[0] && values.billDate[0].format("YYYY-MM-DD") + ' 00:00:00';
                values.endBillDate = values.billDate[1] && values.billDate[1].format("YYYY-MM-DD") + ' 23:59:59';
            }
            if(JSON.parse(values.materialVarietyCode).refpk){
                values.pk_material = JSON.parse(values.materialVarietyCode).refpk;
            }
            delete values.materialVarietyCode;
            
            delete values.billDate;
            for(let attr in values){
                if(!values[attr])
                    delete values[attr];
            }
            this.getQuery(values, 0);
        });
    }
    getQuery = (values, type) => {
        let queryParam = deepClone(this.props.queryParam);
        queryParam.whereParams = values;
        queryParam.pageParams = {
            pageIndex: 0,
            pageSize: 10,
        };
        actions.monitorSample.updateState(queryParam);
        if (type === 0) {
            actions.monitorSample.loadList(queryParam);
        }
    }
    /**
     * 重置
     */
    reset = () => {
        let {queryParam} = this.props;
        queryParam.whereParams = {
            vbillcode:"",
            samplingArea:"",
            isAddPumping:"",
            auditStatus:"Y",
            billDate:"",
            materialVariety:"",
            pk_material:"",
        };
        actions.monitorSample.updateState(queryParam);
    }

    initValueString = (name,pk) => {
        let param = {
            refname:name,
            refpk:pk
        }

        return JSON.stringify(param);
    }

    render(){
        const { getFieldProps, getFieldError } = this.props.form;
        let { orderTypes,queryParam } = this.props;
        let self = this;
        let {
            vbillcode,
            samplingArea,
            isAddPumping,
            auditStatus,
            billDate,
            materialVariety,
            pk_material,
        } = queryParam.whereParams || {};
        return (
            <SearchPanel
                className='search-form'
                form={this.props.form}
                reset={this.reset}
                search={this.search}>
                    <div className='demo-body'>
                        <Form>
                            <Row>
                                 <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('vbillcode', {
                                                    initialValue: vbillcode,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>物料品种</Label>
                                        <RefMultipleTableWithInput
                                            title={'物料'}

                                            param={{
                                                "id": "物料",
                                                "sysId":"type1"
                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/materialref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/materialref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('materialVarietyCode', {
                                                initialValue: self.initValueString(materialVariety,pk_material),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>抽样区</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('samplingArea', {
                                                    initialValue: samplingArea,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>是否已加抽</Label>
                                        <Select
                                            className="audit-select"
                                            {
                                                    ...getFieldProps('isAddPumping', {
                                                    initialValue: isAddPumping || "",
                                                })
                                            }
                                            >       
                                                    <Option value="">全部</Option>
                                                    <Option value="~">未操作</Option>
                                                    <Option value="1">已加抽</Option>
                                                    <Option value="2">未加抽</Option>
                                                        
                                            </Select>
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>审核状态</Label>
                                        <Select
                                        className="audit-select"
                                        {
                                                ...getFieldProps('auditStatus', {
                                                initialValue: auditStatus || 'Y',
                                            })
                                        }
                                        >
                                                <Option value="">全部</Option>
                                                <Option value="Y">已审核</Option>
                                                <Option value="N">未审核</Option>
                                                    
                                        </Select>
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单日期</Label>
                                        <RangePicker
                                            format="YYYY-MM-DD"
                                            // locale={locale}
                                            placeholder={'开始 ~ 结束'}
                                            showClear={true}
                                            {...getFieldProps('billDate', {
                                                initialValue: billDate && moment(billDate) || [moment(),moment()],
                                                validateTrigger: 'onBlur'
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                
                            </Row>
                        </Form>
                    </div>
            </SearchPanel>
        )
    }
}

export default Form.createForm()(MonitorSampleForm)