import React, { Component } from 'react';
import { actions } from 'mirrorx';
import { Button,Row,Col,Label,Select,Message,FormControl,Modal,InputNumber } from 'tinper-bee';
import DatePicker from 'bee-datepicker';
const { RangePicker,MonthPicker } = DatePicker;
import Form from 'bee-form';
const FormItem = Form.FormItem;
import './index.less';

const format = "YYYY-MM-DD";

class ExportModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            modalSize: '',
            searchValues:{}
        };

    }

    componentWillReceiveProps(nextProps) {
        //let {selectData} = nextProps;
        // if(selectData.length == 1) {
        //     this.setState({
        //         bthDisabled:selectData[0].auditStatus
        //     })
        // }
        // if(selectData.length == 0) {
        //     this.setState({
        //         bthDisabled:'N'
        //     })
        // }
    }

    search = (error,values) => {
        let {vertificationSearchCall,selectData} = this.props;
        let self = this;
        this.props.form.validateFields(async (err, values) => {
            if(err) {
                return;
            }
            if(!values.rowNo || !values.columnNo || !values.totalPackageNo || !values.layerNo) {
                Message.create({content: "添加失败！数据不完整", color: 'warning', duration: 3});
                return;
            }
            values.pk_poundbill = selectData[0].pk_poundbill;
            await actions.monitorSample.VerificationCargo(values);
            self.setState({showModal:!self.state.showModal});
            if(vertificationSearchCall) {
                vertificationSearchCall()
            }
        });


    }

    close = () => {
        const {modalcall} = this.props;
        this.setState({showModal:!this.state.showModal});
        if(modalcall) {
            modalcall();
        }
    }

    open = () => {
        let {selectData} = this.props;
        if(selectData.length == 0) {
            Message.create({content: "请选中一条数据", color: 'warning', duration: 3});
            return;
        }
        if(selectData.length > 1) {
            Message.create({content: "只能选中一条数据", color: 'warning', duration: 3});
            return;
        }

        this.setState({
            showModal: true,
        });
    }

    changeSize = (size) => {
        this.setState({
            modalSize: size
        });
    }

    render() {
        const {personList,selectData,disabledFlag} = this.props
        const { getFieldProps, getFieldError } = this.props.form;
        let {bthDisabled} = this.state;
        let column = [];
        let {
            columnNo,
            rowNo,
            layerNo,
            totalPackageNo
        } = selectData[0] || {};
        
        return (

            <span style={{paddingLeft:15}}>
                <Button disabled={disabledFlag} colors="primary" size="sm" onClick={this.open}>核查货物总数</Button>
                <Modal className="custom-modal" show={ this.state.showModal } onHide={ this.close } autoFocus={false}>
                    <Modal.Header closeButton>
                        <Modal.Title > 核查货物总数 </Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                        <Row>
                            <Col md={6}>
                                <FormItem>
                                    <Label>列数</Label>
                                    <InputNumber
                                            {
                                            ...getFieldProps('columnNo', {
                                                initialValue: columnNo || '',
                                            })
                                        }
                                    />
                                    <span style={{
                                        color: 'red'
                                    }}>
                                        {
                                            getFieldError('columnNo')
                                        }
                                    </span>
                                </FormItem>
                            </Col>
                            <Col md={6}>
                                <FormItem>
                                    <Label>排数</Label>
                                    <InputNumber
                                            {
                                            ...getFieldProps('rowNo', {
                                                initialValue: rowNo || '',
                                            })
                                        }
                                    />
                                    <span style={{
                                        color: 'red'
                                    }}>
                                        {
                                            getFieldError('rowNo')
                                        }
                                    </span>
                                </FormItem>
                            </Col>
                            <Col md={6}>
                                <FormItem>
                                    <Label>层数</Label>
                                    <InputNumber
                                            {
                                            ...getFieldProps('layerNo', {
                                                initialValue: layerNo || '',
                                            })
                                        }
                                    />
                                    <span style={{
                                        color: 'red'
                                    }}>
                                        {
                                            getFieldError('layerNo')
                                        }
                                    </span>
                                </FormItem>
                            </Col>
                            <Col md={6}>
                                <FormItem>
                                    <Label>总包数</Label>
                                    <InputNumber
                                            {
                                            ...getFieldProps('totalPackageNo', {
                                                initialValue: totalPackageNo || '',
                                            })
                                        }
                                    />
                                    <span style={{
                                        color: 'red'
                                    }}>
                                        {
                                            getFieldError('totalPackageNo')
                                        }
                                    </span>
                                </FormItem>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={ this.close }> 取消 </Button>
                        <Button style={{marginLeft:10}} colors="primary" onClick={ this.search }> 重新抽样 </Button>
                    </Modal.Footer>
                </Modal>
            </span>
        )
    }
}

export default Form.createForm()(ExportModal)