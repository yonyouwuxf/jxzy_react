import React, { Component } from 'react';
import { actions } from 'mirrorx';
import { Button,Row,Col,Label,Select,Message,FormControl,Modal,InputNumber,Radio } from 'tinper-bee';
import DatePicker from 'bee-datepicker';
const { RangePicker,MonthPicker } = DatePicker;
import Form from 'bee-form';
const FormItem = Form.FormItem;
import './index.less';

const format = "YYYY-MM-DD";


class SampleAddedModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            modalSize: '',
            selectedValue:"左",
            sampleData: [],
            searchValues:{},
        };
    }

    search = (error,values) => {
        let {searchCall,selectData,sampleSearchCall} = this.props;
        let {sampleData} = this.state;
        let self = this;
        this.props.form.validateFields(async (err, values) => {
            if(err) {
                return;
            }
            if(self.state.sampleData.length==0) {
                Message.create({content: "请先添加样本", color: 'warning', duration: 3});
                return;
            }

            selectData[0].pumpingPosition = sampleData;
            // delete selectData[0]._checked;
            await actions.monitorSample.OnResampling(selectData[0]);
            if(sampleSearchCall) {
                sampleSearchCall()
            }
            self.setState({showModal:!self.state.showModal});
        });
    }

    confirmSample = (error,values) => {
        let {searchCall} = this.props;
        let {sampleData} = this.state;
        let self = this;
        this.props.form.validateFields(async (err, values) => {
            if(err) {
                return;
            }

            if(!values.rowNo || !values.columnNo) {
                Message.create({content: "添加失败！数据不完整", color: 'warning', duration: 3});
                return;
            }

            let str = `${values.rowNo}排${values.columnNo}列`;
            sampleData.push(str);
            self.setState({sampleData});
        });
    }

    close = () => {
        const {modalcall} = this.props;
        this.setState({showModal:!this.state.showModal});
        if(modalcall) {
            modalcall();
        }
    }

    open = () => {
        let {selectData} = this.props;
        if(selectData.length == 0) {
            Message.create({content: "请选中一条数据", color: 'warning', duration: 3});
            return;
        }
        if(selectData.length > 1) {
            Message.create({content: "只能选中一条数据", color: 'warning', duration: 3});
            return;
        }
        this.setState({
            showModal: true,
            sampleData:[]
        });
    }

    changeSize = (size) => {
        this.setState({
            modalSize: size
        });
    }
    handleChange = (value) => {
        this.setState({selectedValue: value});
    }

    delSampleData = (index) => {
        let {sampleData} = this.state;
        sampleData.splice(index);
        this.setState({sampleData});
    }

    renderSampleData = () => {
        let {sampleData} = this.state;
        let self = this;
       let dom=[];
        sampleData.map(function(item,index) {
            dom.push(<span>{item} <i className="uf uf-close" onClick={() => {self.delSampleData(index)}}></i></span>);
        })
        return <div>{dom}</div>;
    }

    render() {

        const { getFieldProps, getFieldError } = this.props.form;
        let {bthDisabled} = this.state;
        let {selectData,disabledFlag} = this.props;
        let column = [];
        let self = this;
        let {
            sampleData
        } = this.state;

        let {
            columnNo,
            rowNo,
        } = selectData[0] || {};
        console.log("SampleAddedModal")
        console.log(disabledFlag);
        return (

            <span style={{paddingLeft:15}}>
                <Button disabled={disabledFlag} colors="primary" size="sm" onClick={this.open}>加抽样本</Button>
                <Modal className="custom-modal" show={ this.state.showModal } onHide={ this.close } autoFocus={false}>
                    <Modal.Header closeButton>
                        <Modal.Title > 加抽样本 </Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                            <Row>
                                <Col md={4}>
                                    <FormItem>
                                        <Label>排数</Label>
                                        <InputNumber
                                                {
                                                ...getFieldProps('rowNo', {
                                                    initialValue: '',
                                                })
                                            }
                                        />
                                        <span style={{
                                            color: 'red'
                                        }}>
                                            {
                                                getFieldError('rowNo')
                                            }
                                        </span>
                                    </FormItem>
                                </Col>
                                <Col md={4}>
                                    <FormItem>
                                        <Label>列数</Label>
                                        <InputNumber
                                                {
                                                ...getFieldProps('columnNo', {
                                                    initialValue: '',
                                                })
                                            }
                                        />
                                        <span style={{
                                            color: 'red'
                                        }}>
                                            {
                                                getFieldError('columnNo')
                                            }
                                        </span>
                                    </FormItem>
                                </Col>
                                
                                
                                <Col md={4}>
                                    <Button style={{marginTop:10}} colors="primary" size="sm" onClick={this.confirmSample}>添加</Button>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="sampleDiv">
                                    {this.renderSampleData()}
                                </Col>
                            </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={ this.close }> 取消 </Button>
                        <Button style={{marginLeft:10}} colors="primary" onClick={ this.search }> 加抽样本 </Button>
                    </Modal.Footer>
                </Modal>
            </span>
        )
    }
}

export default Form.createForm()(SampleAddedModal)