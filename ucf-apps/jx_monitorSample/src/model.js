import { actions } from "mirrorx";
// 引入services，如不需要接口请求可不写
import * as api from "./service";
// 接口返回数据公共处理方法，根据具体需要
import { processData } from "utils";
import moment from 'moment';

/**
 *          btnFlag为按钮状态，新增、修改是可编辑，查看详情不可编辑，
 *          新增表格为空
 *          修改需要将行数据带上并显示在卡片页面
 *          查看详情携带行数据但是表格不可编辑
 *          0表示新增、1表示编辑，2表示查看详情 3提交
 *async loadList(param, getState) {
 *          rowData为行数据
*/


export default {
    // 确定 Store 中的数据模型作用域
    name: "monitorSample",
    // 设置当前 Model 所需的初始化 state
    initialState: {
        rowData:{},
        showLoading:false,
        list: [],
        orderTypes:[],
        pageIndex:1,
        pageSize:5,
        totalPages:1,
        total:0,
        detail:{},
        queryParam: {
            pageParams: {
                pageIndex: 0,
                pageSize: 10,
            },
            whereParams: {}
        },
        searchParam:{},
        validateNum:99,//不存在的step

    },
    reducers: {
        /**
         * 纯函数，相当于 Redux 中的 Reducer，只负责对数据的更新。
         * @param {*} state
         * @param {*} data
         */
        updateState(state, data) { //更新state
            return {
                ...state,
                ...data
            };
        }
    },
    effects: {
        /**
         * 加载列表数据
         * @param {*} param
         * @param {*} getState
         */
        async loadList(param, getState) {
            // 正在加载数据，显示加载 Loading 图标
            // 正在加载数据，显示加载 Loading 图标
            actions.monitorSample.updateState({showLoading: true});
            // 调用 getList 请求数据
            let _param = param || getState().monitorSample.queryParam;
            let res = processData(await api.getList(param));
            //let {data:res}=result;
            let _state = {
                showLoading: false,
                queryParam: _param //更新搜索条件
            }
            if (res) {
                _state = Object.assign({}, _state, {
                    list: res.content,
                    pageIndex: res.number + 1,
                    totalPages: res.totalPages,
                    total: res.totalElements,
                    pageSize: res.size,
                })
            }
            actions.monitorSample.updateState(_state);
        },

        /**
         * getSelect：获取下拉列表数据
         * @param {*} param
         * @param {*} getState
         */
        getOrderTypes(param,getState){
            actions.monitorSample.updateState({
            orderTypes:  [{
                "code":"0",
                "name":"D001"
            },{
                "code":"1",
                "name":"D002"
            },{
                "code":"2",
                "name":"D003"
            },{
                "code":"3",
                "name":"D004"
            }]
            })
        },

        /**
         * getSelect：保存table数据
         * @param {*} param
         * @param {*} getState
         */
        async saveList(param, getState) {
            let result = await api.saveList(param);
            return result;
        },
        /**
         * 删除table数据
         * @param {*} id
         * @param {*} getState
         */
        async removeList(id, getState) {
            let result = await api.deleteList([{id}]);
            return result;
        },

        async OnResampling(param,getState){
            actions.monitorSample.updateState({
              showLoading:true
            })
            processData(await api.OnResampling(param),'重新抽样成功');

            actions.monitorSample.updateState({
              showLoading:false
            })
        },

        async VerificationCargo(param,getState){
            actions.monitorSample.updateState({
              showLoading:true
            })
            processData(await api.VerificationCargo(param),'操作成功');


            actions.monitorSample.updateState({
              showLoading:false
            })
        },

        async save(param,getState){//保存
            actions.monitorSample.updateState({
              showLoading:true
            })
            let res = processData(await api.saveMonitorSample(param),'保存成功');
            console.log("保存信息",res);
            if(res){
               window.history.go(-1);
            }
            actions.monitorSample.updateState({
                showLoading:false,

            });
        },

        async delItem(param,getState){
            actions.monitorSample.updateState({
              showLoading:true
            })
            let res=processData(await api.delMonitorSample(param.param),'删除成功');
            actions.monitorSample.loadList();
        },

        async confirmCancelPumping(param,getState){
            actions.monitorSample.updateState({
              showLoading:true
            })
            let res=processData(await api.confirmCancelPumping(param),'操作成功');
        },

        async confirmVerify(param,getState){
            actions.monitorSample.updateState({
              showLoading:true
            })
            let res=processData(await api.confirmVerify(param),'操作成功');
        },

        async humanVerify(param,getState){
            actions.monitorSample.updateState({
              showLoading:true
            })
            let res=processData(await api.humanVerify(param),'操作成功');
        },

        async queryDetail(param,getState) {
            let {data:{detailMsg:{data:{content}}}}=await api.getDetail(param);
            return content[0];
        },

        async printExcel(param) {
            let res=processData(await api.queryPrintTemplateAllocate(param.queryParams),'');
            if(!res || !res.res_code) return false;
            await api.printExcel({
                tenantId: 'tenant',
                printcode: res.res_code,
                serverUrl: `${GROBAL_HTTP_CTX}/jx_monitorSample/dataForPrint`,
                params: encodeURIComponent(JSON.stringify(param.printParams)),
                sendType: 'post'
            })
        },
        
        async exportExcel(param) {
            api.exportExcel(param || {});
        },


    }
};