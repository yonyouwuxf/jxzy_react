import React from 'react';
import mirror, { connect } from 'mirrorx';

// 组件引入
import MonitorSampleTable from './components/monitorSample-root/MonitorSampleTable';
import MonitorSampleSelectTable from './components/monitorSample-root/MonitorSampleSelectTable';
import MonitorSamplePaginationTable from './components/monitorSample-root/MonitorSamplePaginationTable';
import MonitorSampleEdit from './components/monitorSample-edit/Edit';
import MonitorSampleBpmChart from './components/monitorSample-bpm-chart'
// 数据模型引入
import model from './model'
mirror.model(model);

// 数据和组件UI关联、绑定
export const ConnectedMonitorSampleTable = connect( state => state.monitorSample, null )(MonitorSampleTable);
export const ConnectedMonitorSampleSelectTable = connect( state => state.monitorSample, null )(MonitorSampleSelectTable);
export const ConnectedMonitorSamplePaginationTable = connect( state => state.monitorSample, null )(MonitorSamplePaginationTable);
export const ConnectedMonitorSampleEdit = connect( state => state.monitorSample, null )(MonitorSampleEdit);
export const ConnectedMonitorSampleBpmChart = connect( state => state.monitorSample, null )(MonitorSampleBpmChart);
