import React, { Component } from 'react'
import { actions } from "mirrorx";
import { Switch, InputNumber, Col, Row,FormControl, Label, Select,Form,Radio } from "tinper-bee";
import SearchPanel from 'components/SearchPanel'
import DatePicker from "tinper-bee/lib/Datepicker";
// import RefMultipleTableWithInput from 'pap-refer/lib/ref-multiple-table/src/index';
// import 'pap-refer/lib/ref-multiple-table/src/index.css';
import {RefMultipleTableWithInput} from 'pap-refer/dist/index';
// import {RefTable} from 'components/RefViews'
 import 'pap-refer/dist/index.css';

import { deepClone } from "utils";
import moment from 'moment';
const { RangePicker } = DatePicker;
const FormItem = Form.FormItem;
import './index.less'

class DomesticPaperForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            pk_domesticPaper: '',
        }
    }
    componentWillMount(){
        // 获得国内废纸称重列表数据
        actions.domesticPaper.getOrderTypes();
    }
    /** 查询数据
     * @param {*} error 校验是否成功
     * @param {*} values 表单数据
     */
    search = (error,values) => {
        this.props.form.validateFields(async (err, values) => {
            if(values.billDate[0]) {
                values.startBillDate = values.billDate[0] && values.billDate[0].format("YYYY-MM-DD") + ' 00:00:00';
                values.endBillDate = values.billDate[1] && values.billDate[1].format("YYYY-MM-DD") + ' 23:59:59';
            }
            if(values.mainSupplier && JSON.parse(values.mainSupplier).refpk){
                values.pk_mainSupplier = JSON.parse(values.mainSupplier).refpk;
            }
            if(values.materialVarietyCode && JSON.parse(values.materialVarietyCode).refpk){
                values.pk_material = JSON.parse(values.materialVarietyCode).refpk;
            }
            if(values.orgCode && JSON.parse(values.orgCode).refpk){
                values.orgNo = JSON.parse(values.orgCode).refpk;
                //values.orgName = JSON.parse(values.materialVarietyCode).refName;
            }

            if(JSON.parse(values.heapNocode).refpk){
                values.heapNo = JSON.parse(values.heapNocode).refpk;
                values.heapName = JSON.parse(values.heapNocode).refname;
            }

            delete values.heapNocode;

            delete values.mainSupplier;
            delete values.materialVarietyCode;
            delete values.billDate;
            delete values.orgCode;
            for(let attr in values){
                if(!values[attr])
                    delete values[attr];
            }
           
            this.getQuery(values, 0);
        });


    }

    getQuery = (values, type) => {
        let queryParam = deepClone(this.props.queryParam);
        let {pageParams, whereParams} = queryParam;
        queryParam.whereParams = values;
        queryParam.pageParams = {
            pageIndex: 0,
            pageSize: 10,
        };
        actions.domesticPaper.updateState(queryParam);
        if (type === 0) {
            actions.domesticPaper.loadList(queryParam);
        }
        // this.props.onCloseEdit();

    }

    /**
     * 重置
     */
    reset = () => {
        let {queryParam} = this.props;
         queryParam.whereParams = {
            poundNo:"",
            auditStatus:'Y',
            serialNo:"",
            licensePlateNo:"",
            mainSupplier:"",
            weiCompleMark:"",
            startBillDate:"",
            endBillDate:"",
            pk_material:"",
            materialVariety:"",
            qualityGrade:"",
            heapNo:"",
            heapName:"",
            orgCode:"",
            orgNo:"",
            orgName:""
        };

        actions.domesticPaper.updateState(queryParam);
    }
    
    initValueString = (name,pk) => {
        let param = {
            refname:name,
            refpk:pk
        }

        return JSON.stringify(param);
    }

    render(){
        const { getFieldProps, getFieldError } = this.props.form;
        let { orderTypes,queryParam } = this.props;
        let self = this;
        let {
            poundNo,
            auditStatus,
            serialNo,
            licensePlateNo,
            mainSupplier,
            weiCompleMark,
            startBillDate,
            endBillDate,
            pk_material="",
            materialVariety="",
            heapNo="",
            heapName="",
            qualityGrade,
            orgName,
            orgNo
        } = queryParam.whereParams || {};

        return (
            <SearchPanel
                className='repaired_file-form'
                form={this.props.form}
                reset={this.reset}
                search={this.search}>
                    <div className='demo-body'>
                        <Form>
                            <Row>
                            <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>采购组织</Label>
                                        <RefMultipleTableWithInput
                                            title={'采购组织'}

                                            param={{
                                                "id": "采购组织",
                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/OrgRef/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/OrgRef/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('orgCode', {
                                                initialValue: self.initValueString(orgName,orgNo),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                 <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('poundNo', {
                                                    initialValue: poundNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>堆号</Label>
                                        <RefMultipleTableWithInput
                                            title={'堆号'}

                                            param={{
                                                "id": "堆号",

                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/rackref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/rackref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('heapNocode', {
                                                initialValue: self.initValueString(heapName,heapNo),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>审核状态</Label>
                                        <Select
                                        className="audit-select"
                                        {
                                                ...getFieldProps('auditStatus', {
                                                initialValue: auditStatus || 'Y',
                                            })
                                        }
                                        >
                                                <Option value="">请选择</Option>
                                                <Option value="Y">已审核</Option>
                                                <Option value="N">未审核</Option>
                                                    
                                        </Select>
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>称重完成标记</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('weiCompleMark', {
                                                    initialValue: weiCompleMark,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>主供应商</Label>
                                        <RefMultipleTableWithInput
                                            title={'主供应商'}

                                            param={{
                                                "id": "主供应商",

                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/suppref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/suppref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={false}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('mainSupplier', {
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>物料品种</Label>
                                        <RefMultipleTableWithInput
                                        title={'物料'}

                                        param={{
                                            "id": "物料",
                                            "sysId":"type1"
                                        }}
                                        refModelUrl={{
                                            tableBodyUrl: '/jxzy/materialref/blobRefSearch',//表体请求
                                            refInfo: '/jxzy/materialref/getRefModelInfo',//表头请求
                                        }}
                                        matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                        filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                        multiple={true}
                                        searchable={true}
                                        checkStrictly= {true}
                                        strictMode = {true}
                                        
                                        miniSearch={false}
                                        displayField='{refname}'
                                        valueField='refid'

                                        {...getFieldProps('materialVarietyCode', {
                                            initialValue: self.initValueString(materialVariety,pk_material),
                                        })}
                                    />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>车牌号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('licensePlateNo', {
                                                    initialValue: licensePlateNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单日期</Label>
                                        <RangePicker
                                            format="YYYY-MM-DD"
                                            // locale={locale}
                                            placeholder={'开始 ~ 结束'}
                                            showClear={true}
                                            {...getFieldProps('billDate', {
                                                initialValue: startBillDate && [moment(startBillDate),moment(endBillDate)] || [moment(),moment()],
                                                validateTrigger: 'onBlur'
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>序列号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('serialNo', {
                                                    initialValue: serialNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                {/* <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>质量等级</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('qualityGrade', {
                                                    initialValue: qualityGrade,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col> */}
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>质量等级</Label>
                                        <Select
                                        className="audits-select"
                                        {
                                                ...getFieldProps('qualityGrade', {
                                                initialValue: qualityGrade || '',
                                            })
                                        }
                                        >
                                                <Option value=''>请选择</Option>
                                                <Option value="AJ挂面纸">AJ挂面纸</Option>
                                                <Option value="挂面纸">挂面纸</Option>
                                                <Option value="AJ黄板">AJ黄板</Option>
                                                <Option value="超市">超市</Option>
                                                <Option value="黄板">黄板</Option>
                                                <Option value="I类">I类</Option>
                                                <Option value="II类">II类</Option>
                                                <Option value="III类">III类</Option>
                                                <Option value="IIII类">IIII类</Option>
                                                <Option value="IIIII类">IIIII类</Option>
                                                <Option value="化纤管">化纤管</Option>
                                                <Option value="保密纸">保密纸</Option>
                                                <Option value="高白">高白</Option>
                                                <Option value="低白">低白</Option>
                                                <Option value="散片AJ黄板">散片AJ黄板</Option>
                                        </Select>
                                    </FormItem>
                                </Col>
                            </Row>
                        </Form>
                    </div>
            </SearchPanel>
           
        )
    }
}

export default Form.createForm()(DomesticPaperForm)