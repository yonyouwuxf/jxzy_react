import React, { Component } from 'react';
import { actions } from 'mirrorx';
import { Button, Row, Col, Label, Select, Message, FormControl, Modal, InputNumber, Radio } from 'tinper-bee';
import DatePicker from 'bee-datepicker';
const { RangePicker, MonthPicker } = DatePicker;
import Form from 'bee-form';
const FormItem = Form.FormItem;
import './index.less';

const format = "YYYY-MM-DD";


class ExportImgNew extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            modalSize: '',
            selectedValue: "左",
            sampleData: [],
            searchValues: {},
        };

    }

    search = (error, values) => {
        let { searchCall, selectData, cancelSelect } = this.props;
        let self = this;
        this.props.form.validateFields(async (err, values) => {
            if (err) {
                return;
            }
            if (values.ds && values.de)
                window.open(`${GROBAL_HTTP_CTX}/file_operation/downfcpbf?ds=${values.ds.format("YYYY-MM-DD")}&de=${values.de.format("YYYY-MM-DD")}&bdh=${values.bdh}&gys=${values.gys}`)
            else
                alert("请先选择起止时间")
        });
    }

    close = () => {
        const { modalcall } = this.props;
        this.setState({ showModal: !this.state.showModal });
        if (modalcall) {
            modalcall();
        }
    }

    open = () => {

        this.setState({
            showModal: true
        });
    }

    changeSize = (size) => {
        this.setState({
            modalSize: size
        });
    }

    render() {

        const { getFieldProps, getFieldError } = this.props.form;
        let column = [];
        let self = this;
        let {
            sampleData
        } = this.state;

        return (

            <span style={{ paddingLeft: 20 }}>
                <Button colors="primary" size="sm" onClick={this.open}>导出抓拍</Button>
                <Modal className="custom-modal" size="lg" show={this.state.showModal} onHide={this.close} autoFocus={false}>
                    <Modal.Header closeButton>
                        <Modal.Title > 导出抓拍 </Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                        <Row>
                            <Col md={8}>
                                <FormItem>
                                    <Label>起始时间 </Label>
                                    <DatePicker
                                        format="YYYY-MM-DD"
                                        // locale={locale}
                                        placeholder={'起始时间'}
                                        showClear={true}
                                        {...getFieldProps('ds', {
                                            // initialValue: startBillDate && [moment(startBillDate),moment(endBillDate)] || [moment(),moment()],
                                            // validateTrigger: 'onBlur'
                                        })}
                                    />
                                </FormItem>
                            </Col>
                            <Col md={8}>
                                <FormItem>
                                    <Label>结束时间 </Label>
                                    <DatePicker
                                        format="YYYY-MM-DD"
                                        // locale={locale}
                                        placeholder={'结束时间'}
                                        showClear={true}
                                        {...getFieldProps('de', {
                                            // initialValue: startBillDate && [moment(startBillDate),moment(endBillDate)] || [moment(),moment()],
                                            // validateTrigger: 'onBlur'
                                        })}
                                    />
                                </FormItem>
                            </Col>
                            <Col md={8}>
                                <FormItem>
                                    <Label>磅单号 </Label>
                                    <FormControl
                                        {
                                        ...getFieldProps('bdh', {
                                            initialValue: '',
                                        })
                                        } />
                                </FormItem>
                            </Col>
                            <Col md={8}>
                                <FormItem>
                                    <Label>供应商名称 </Label>
                                    <FormControl
                                        {
                                        ...getFieldProps('gys', {
                                            initialValue: '',
                                        })
                                        } />
                                </FormItem>
                            </Col>

                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.close}> 取消 </Button>
                        <Button style={{ marginLeft: 10 }} colors="primary" onClick={this.search}> 确认 </Button>
                    </Modal.Footer>
                </Modal>
            </span>
        )
    }
}

export default Form.createForm()(ExportImgNew)