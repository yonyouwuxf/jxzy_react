import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { actions } from "mirrorx";

import Header from 'components/Header';
import DomesticPaperTable from '../domesticPaper-table';
import DomesticPaperForm from '../domesticPaper-form';

import './index.less';

/**
 * DomesticPaperRoot Component
 */
class DomesticPaperRoot  extends Component {
    constructor(props) {
        super(props);
        this.state = { }
    }
    /**
     *
     */
    componentWillMount() {
        this.getTableData();
    }
    /**
     * 获取table表格数据
     */
    getTableData = () => {
        actions.domesticPaper.loadList();
    }

    render() {
        let { pageSize, pageIndex, totalPages} = this.props;
        return (
            <div className='domesticPaper-root'>
                <Header title='国内废纸称重' back={true}/>
                <DomesticPaperForm { ...this.props }/>
                <DomesticPaperTable { ...this.props }/>
            </div>
        )
    }
}
export default DomesticPaperRoot;