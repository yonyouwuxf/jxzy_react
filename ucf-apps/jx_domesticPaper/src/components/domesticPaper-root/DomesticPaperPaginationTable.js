import React, { Component } from 'react'
import PaginationTable from 'components/PaginationTable'
// import {BpmButtonSubmit,BpmButtonRecall} from 'yyuap-bpm';
import { actions } from 'mirrorx';
import { Button, Message, Modal, Loading, Tooltip } from 'tinper-bee';
import Select from 'bee-select';
import moment from "moment/moment";
import Header from 'components/Header';
import DelModal from 'components/DelModal';
import DomesticPaperForm from '../domesticPaper-form';
import ExportImg from '../custom-modal/ExportImg.js';
// import ExportImgNew from '../custom-modal-new/ExportImgNew.js';
import AcExport from '../domesticPaper-export';
import { deepClone, getPageParam } from "utils";
import './index.less'
export default class DomesticPaperPaginationTable extends Component {
    constructor(props) {
        super(props);
        let self = this;
        this.state = {
            // 表格中所选中的数据，拿到后可以去进行增删改查
            step: 10,
            showModal: false,
            delData: [],
            searchValues: {},
            selectData: [],
            column: [
                {
                    title: "辅供货商",
                    dataIndex: "auxiliarySupplier",
                    key: "auxiliarySupplier",
                    width: 150,
                    render: (text, record, index) => {
                        return (
                            <Tooltip inverse overlay={text}>
                                <span tootip={text} className="popTip">{text}</span>
                            </Tooltip>
                        );
                    }
                }, {
                    title: "质量等级",
                    dataIndex: "qualityGrade",
                    key: "qualityGrade",
                    width: 80,
                }, {
                    title: "毛重",
                    dataIndex: "grossWeight",
                    key: "grossWeight",
                    width: 80,
                }, {
                    title: "皮重",
                    dataIndex: "tareWeight",
                    key: "tareWeight",
                    width: 80,
                }, {
                    title: "货重",
                    dataIndex: "settlementWeight",
                    key: "settlementWeight",
                    width: 80,
                }, {
                    title: "含水分比",
                    dataIndex: "averageWaterRatio",
                    key: "averageWaterRatio",
                    width: 80,
                }, {
                    title: "含杂质比",
                    dataIndex: "averageImpurityRatio",
                    key: "averageImpurityRatio",
                    width: 80,
                }, {
                    title: "水分另扣",
                    dataIndex: "averageDewateringWeight",
                    key: "averageDewateringWeight",
                    width: 80,
                }, {
                    title: "杂质另扣",
                    dataIndex: "averageimpurityWeight",
                    key: "averageimpurityWeight",
                    width: 100,
                }, {
                    title: "结算重量",
                    dataIndex: "netWeight",
                    key: "netWeight",
                    width: 80,
                }, {
                    title: "单价",
                    dataIndex: "averageUnitPrice",
                    key: "averageUnitPrice",
                    width: 80,
                }, {
                    title: "价格系数",
                    dataIndex: "priceCoefficient",
                    key: "priceCoefficient",
                    width: 80,
                }, {
                    title: "是否人工检测",
                    dataIndex: "humanVerifyFlag",
                    key: "humanVerifyFlag",
                    width: 100,
                    render: function (text) {
                        if (text == "true") {
                            return "是";
                        } else {
                            return "否";
                        }
                    }
                }, {
                    title: "单件重",
                    dataIndex: "singlePieceWeight",
                    key: "singlePieceWeight",
                    width: 80,
                }, {
                    title: "总件数",
                    dataIndex: "totalPackageNo",
                    key: "totalPackageNo",
                    width: 80,
                }, {
                    title: "开包件数",
                    dataIndex: "unpackNo",
                    key: "unpackNo",
                    width: 80,
                }, {
                    title: "堆号",
                    dataIndex: "heapName",
                    key: "heapName",
                    width: 120,
                }, {
                    title: "车牌号",
                    dataIndex: "licensePlateNo",
                    fixed: "left",
                    key: "licensePlateNo",
                    width: 100
                }, {
                    title: "序列号",
                    dataIndex: "serialNo",
                    fixed: "left",
                    key: "serialNo",
                    width: 100,
                }, {
                    title: "物料品种",
                    dataIndex: "materialVariety",
                    fixed: "left",
                    key: "materialVariety",
                    width: 200,
                }, {
                    title: "主供货商",
                    dataIndex: "mainSupplier",
                    fixed: "left",
                    key: "mainSupplier",
                    width: 150,
                    render: (text, record, index) => {
                        return (
                            <Tooltip inverse overlay={text}>
                                <span tootip={text} className="popTip">{text}</span>
                            </Tooltip>
                        );
                    }

                }, {
                    title: "磅单号",
                    dataIndex: "poundNo",
                    key: "poundNo",
                    width: 120,
                }, {
                    title: "审核状态",
                    dataIndex: "auditStatus",
                    key: "auditStatus",
                    width: 80,
                    render(text, record, index) {
                        if (text == "Y") {
                            return "审核完成"
                        } else {
                            return "未审核"
                        }
                    }
                }, {
                    title: "操作",
                    dataIndex: "d",
                    key: "d",
                    width: 100,
                    fixed: "right",
                    render(text, record, index) {
                        return (
                            <div className='operation-btn'>
                                <i size='sm' className='uf uf-search edit-btn' onClick={() => { self.cellClick(record, 2) }}></i>
                                {record.auditStatus != 'Y' && <i size='sm' className='uf uf-pencil edit-btn' onClick={() => { self.cellClick(record, 1) }}></i>}
                            </div>
                        )
                    }
                }
            ]
        }
    }

    componentDidMount() {
        actions.domesticPaper.loadList(this.props.queryParam);

    }
    /**
     * 编辑,详情，增加
     */

    cellClick = async (record, btnFlag) => {
        await actions.domesticPaper.updateState({
            rowData: record,
        });

        let id = "";
        if (record) {
            id = record["id"];
        }
        actions.routing.push(
            {
                pathname: 'domesticPaper-edit',
                search: `?search_id=${id}&btnFlag=${btnFlag}`
            }
        )
    }

    // 删除操作
    delItem = (record, index) => {
        this.setState({
            showModal: true,
            delData: [{ id: record.id, ts: record.ts }]
        });

    }

    // 表格勾选回调函数，返回选中数据
    onTableSelectedData = data => {

        this.setState({ selectData: data })
    }
    // 分页单页数据条数选择函数
    onPageSizeSelect = (index, value) => {
        let queryParam = deepClone(this.props.queryParam);
        let { pageIndex, pageSize } = queryParam.pageParams;
        pageSize = value;
        queryParam['pageParams'] = { pageIndex, pageSize };
        actions.domesticPaper.loadList(queryParam);
    }

    // 分页组件点击页面数字索引执行函数
    onPageIndexSelect = (value, type) => {
        let queryParam = deepClone(this.props.queryParam);
        let { pageIndex, pageSize } = queryParam.pageParams;
        pageIndex = value - 1;

        //let { pageIndex, pageSize } = getPageParam(value, type, queryParam.pageParams);
        queryParam['pageParams'] = { pageIndex, pageSize };
        this.setState({ selectedIndex: 0 }); //默认选中第一行
        actions.domesticPaper.loadList(queryParam);
    }


    searchValuesCall = (value) => {
        actions.domesticPaper.updateState(this.props.queryParam);
    }

    // 模态框确认删除
    onModalDel = async (delFlag) => {
        let { delData } = this.state;
        if (delFlag) {
            await actions.domesticPaper.delItem({
                param: delData
            });
        }
        this.setState({
            showModal: false,
            delData: []
        })
    }
    // 模板下载
    onLoadTemplate = () => {
        window.open(`${GROBAL_HTTP_CTX}/jx_domesticPaper/excelTemplateDownload`)
    }

    onConfirmVerify = () => {
        let { selectData } = this.state;
        actions.domesticPaper.Verify({
            param: selectData
        });
        actions.domesticPaper.loadList(this.props.queryParam);
        this.setState({ selectData: [] });
    }

    exportValuesCall = () => {
        this.setState({ selectData: [] });
    }

    onConfirmCancelVerify = () => {
        let { selectData } = this.state;
        actions.domesticPaper.CancelVerify({
            param: selectData
        });
        actions.domesticPaper.loadList(this.props.queryParam);
        this.setState({ selectData: [] });
    }

    exportValuesCall = () => {
        let { queryParam } = this.props;
        actions.domesticPaper.loadList(queryParam);
    }

    render() {
        const self = this;
        let { list, showLoading, pageIndex, pageSize, totalPages, total, queryParam } = this.props;
        let { showModal, selectData } = this.state;
        let exportProps = { total, pageIndex, pageSize, selectData, list, queryParam };
        // 将boolean类型数据转化为string
        list.forEach(function (item) {
            Object.keys(item).forEach(function (key) {
                if (typeof item[key] === 'boolean') {
                    item[key] = String(item[key]);
                }
            })
        })



        return (
            <div className='domesticPaper-root'>
                <DomesticPaperForm  {...this.props} />

                <div className='table-header'>

                    <DelModal operType={'audittrue'} batch={true} modalTitle="审核" selectData={selectData} modalContent="确认审核通过" confirmFn={this.onConfirmVerify} >
                        <Button colors="primary" style={{ "marginLeft": 15 }} size='sm' >
                            审核
                        </Button>
                    </DelModal>

                    <DelModal operType={'auditfalse'} batch={true} modalTitle="取消审核" selectData={selectData} modalContent="确认取消审核通过" confirmFn={this.onConfirmCancelVerify} >
                        <Button colors="primary" style={{ "marginLeft": 15 }} size='sm' >
                            取消审核
                        </Button>
                    </DelModal>
                    <AcExport exportValuesCall={this.exportValuesCall} {...exportProps} className="ml5" />
                    <ExportImg />
                    {/* <ExportImgNew /> */}
                </div>
                <PaginationTable
                    data={list}
                    rowClassName={(record, index, indent) => {
                        if (record.auditStatus == 'Y') {
                            return 'selected';
                        } else {
                            return 'aa';
                        }
                    }}
                    pageIndex={pageIndex}
                    pageSize={pageSize}
                    totalPages={totalPages}
                    total={total}
                    columns={this.state.column}
                    checkMinSize={6}
                    getSelectedDataFunc={this.tabelSelect}
                    onTableSelectedData={this.onTableSelectedData}
                    onPageSizeSelect={this.onPageSizeSelect}
                    onPageIndexSelect={this.onPageIndexSelect}
                />
                <Loading show={showLoading} loadingType="line" />
                <Modal
                    show={showModal}
                    onHide={this.close} >
                    <Modal.Header>
                        <Modal.Title>确认删除</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        是否删除选中内容
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={() => this.onModalDel(false)} shape="border" style={{ marginRight: 50 }}>关闭</Button>
                        <Button onClick={() => this.onModalDel(true)} colors="primary">确认</Button>
                    </Modal.Footer>
                </Modal>
            </div>

        )

    }
}