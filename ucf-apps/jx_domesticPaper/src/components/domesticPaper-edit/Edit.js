import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { actions } from "mirrorx";
import queryString from 'query-string';
import { Collapse, Switch, InputNumber, Loading, Table, Button, Col, Row, Icon, InputGroup, FormControl, Checkbox, Modal, Panel, PanelGroup, Label, Message } from "tinper-bee";
import Radio from 'bee-radio';
// import { BpmTaskApprovalWrap } from 'yyuap-bpm';
import Header from "components/Header";
import DatePicker from 'bee-datepicker';
// import RefMultipleTableWithInput from 'pap-refer/lib/ref-multiple-table/src/index';
// import 'pap-refer/lib/ref-multiple-table/src/index.css';
import { RefMultipleTableWithInput } from 'pap-refer/dist/index';
import 'pap-refer/dist/index.css';
import Form from 'bee-form';
import Select from 'bee-select';
import Viewer from 'react-viewer';
import 'react-viewer/dist/index.css';
import moment from "moment";
import './edit.less';
import { setCookie, getCookie } from "utils";

const FormItem = Form.FormItem;
const Option = Select.Option;
const format = "YYYY-MM-DD HH:mm:ss";




class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rowData: {},
            visibleView: false,
            imgList: [],
            fileNameData: props.rowData.attachment || [],//上传附件数据
            checkList: [],
            imgUrl: []
        }
    }
    async componentWillMount() {
        await actions.domesticPaper.getOrderTypes();
        let searchObj = queryString.parse(this.props.location.search);
        let { btnFlag, search_id } = searchObj;

        let param = {
            id: search_id
        }
        actions.domesticPaper.loadChildList(param);//table数据

        if (btnFlag && btnFlag > 0) {
            let tempRowData = await actions.domesticPaper.queryDetail({ search_id });
            let rowData = this.handleRefShow(tempRowData) || {};

            console.log('rowData', rowData);
            this.setState({
                rowData: rowData,
            })
        }

    }

    save = () => {//保存
        this.props.form.validateFields(async (err, values) => {
            //values.attachment = this.state.fileNameData;
            let numArray = [
            ];
            for (let i = 0, len = numArray.length; i < len; i++) {
                values[numArray[i]] = Number(values[numArray[i]]);
            }


            // if (err) {
            //     Message.create({ content: '数据填写错误', color: 'danger' });
            // } else {
            let { rowData,
            } = this.state;

            if (JSON.parse(values.heapNocode).refpk) {
                values.heapName = JSON.parse(values.heapNocode).refname;
                values.heapNo = JSON.parse(values.heapNocode).refpk;
            }

            if (JSON.parse(values.materialVarietyCode).refpk) {
                values.materialVariety = JSON.parse(values.materialVarietyCode).refname;
                values.pk_material = JSON.parse(values.materialVarietyCode).refpk;
            }

            if (JSON.parse(values.meterPointcode).refpk) {
                values.pk_meterPoint = JSON.parse(values.meterPointcode).refpk;
                values.meterPoint = JSON.parse(values.meterPointcode).refname;
            }


            delete values.mainSupplier;
            delete values.meterPointcode;
            delete values.heapNocode;
            delete values.materialVarietyCode;
            delete values.attachment;
            debugger;
            let saveObj = Object.assign({}, rowData, values);

            await actions.domesticPaper.save(
                saveObj,
            );
            // }
        });
    }

    showView = (data) => {
        this.setState({
            visibleView: true,
            imgList: data
        })
    }

    // 处理参照回显
    handleRefShow = (tempRowData) => {

        let rowData = {};
        if (tempRowData) {

            let {
            } = tempRowData;

            this.setState({
            })
            rowData = Object.assign({}, tempRowData,
                {
                }
            )
        }
        return rowData;
    }

    onBack = async () => {
        window.history.go(-1);
    }

    // 选择照片
    picChecked = (indexOut, index, picPath) => {
        let { checkList, imgUrl } = this.state
        checkList[indexOut][index] = !checkList[indexOut][index]

        if (checkList[indexOut][index])
            imgUrl.push(picPath)
        else
            imgUrl = imgUrl.filter(path => path != picPath)

        this.setState({ checkList, imgUrl });
        console.log(this.state.checkList)
    }

    // 上传照片
    uploadPic = async () => {
        let { imgUrl, rowData } = this.state;

        let param = {
            imgUrl,
            pk_pound: rowData.pk_pound
        }

        console.log(param)

        if (imgUrl && imgUrl.length && imgUrl.length > 0)
            await actions.domesticPaper.uploadPic(param);
    }

    // 动态显示标题
    onChangeHead = (btnFlag) => {
        let titleArr = ["新增", "编辑", "详情"];
        return titleArr[btnFlag] || '新增';
    }

    // 跳转到流程图
    onClickToBPM = (rowData) => {
        console.log("actions", actions);
        actions.routing.push({
            pathname: 'domesticPaper-chart',
            search: `?id=${rowData.id}`
        })
    }

    // 流程图相关回调函数
    onBpmStart = () => {
        actions.domesticPaper.updateState({ showLoading: true });
    }
    onBpmEnd = () => {
        actions.domesticPaper.updateState({ showLoading: false });
    }
    onBpmSuccess = () => {
        window.setTimeout(() => {
            actions.domesticPaper.updateState({ showLoading: false });
            // actions.routing.push('pagination-table');
            actions.routing.goBack();
        }, 1000);
    }
    onBpmError = () => {
        actions.domesticPaper.updateState({ showLoading: false });
    }

    // 审批面板展示
    showBpmComponent = (btnFlag, appType, id, processDefinitionId, processInstanceId, rowData) => {
        // btnFlag为2表示为详情
        if ((btnFlag == 2) && rowData && rowData['id']) {
            console.log("showBpmComponent", btnFlag)
            return (
                <div >
                    {/* {appType == 1 &&<BpmTaskApprovalWrap
                        id={rowData.id}
                        onBpmFlowClick={() => { this.onClickToBPM(rowData) }}
                        appType={appType}
                        onStart={this.onBpmStart}
                        onEnd={this.onBpmEnd}
                        onSuccess={this.onBpmSuccess}
                        onError={this.onBpmError}
                    />}
                    {appType == 2 &&<BpmTaskApprovalWrap
                        id={id}
                        processDefinitionId={processDefinitionId}
                        processInstanceId={processInstanceId}
                        onBpmFlowClick={() => { this.onClickToBPM(rowData) }}
                        appType={appType}
                        onStart={this.onBpmStart}
                        onEnd={this.onBpmEnd}
                        onSuccess={this.onBpmSuccess}
                        onError={this.onBpmError}
                    />} */}
                </div>

            );
        }
    }

    arryDeepClone = (array) => {
        let result = [];
        if (array) {
            array.map((item) => {
                let temp = Object.assign([], item);
                result.push(temp);
            })
        }
    }

    initValueString = (name, pk) => {
        let param = {
            refname: name,
            refpk: pk
        }

        return JSON.stringify(param);
    }

    // 通过search_id查询数据

    render() {
        const self = this;
        let { childList } = this.props;
        let { btnFlag, appType, id, processDefinitionId, processInstanceId } = queryString.parse(this.props.location.search);
        btnFlag = Number(btnFlag);
        let { rowData,
            imgList,
            visibleView,
            checkList,
            imgUrl
        } = this.state;

        if (!checkList.length)
            for (let i of childList) {
                let tmp = new Array(10).fill(false)
                checkList.push(tmp)
            }

        // 将boolean类型数据转化为string
        Object.keys(rowData).forEach(function (item) {
            if (typeof rowData[item] === 'boolean') {
                rowData[item] = String(rowData[item]);
            }
        });

        let columnchild = [
            {
                title: "检验员",
                dataIndex: "inspector",
                key: "inspector",
                width: 100
            },
            {
                title: "检验点",
                dataIndex: "checkPoint",
                key: "checkPoint",
                width: 200
            }, {
                title: "检验时间",
                dataIndex: "inspectTime",
                key: "inspectTime",
                width: 150
            }, {
                title: "单价(元)",
                dataIndex: "unit",
                key: "unit",
                width: 100
            }, {
                title: "含水分比-测水仪(%)",
                dataIndex: "waterRatioMeter",
                key: "waterRatioMeter",
                width: 100
            }, {
                title: "检验照片",
                dataIndex: "imgUrl",
                key: "imgUrl",
                width: 200,
                render: function (text, record, indexOut) {
                    if (!text) return <div></div>;

                    return <div className="monitor-imgList-wrapper">
                        {
                            text.map((item, index) => {
                                return <div className="monitor-img-wrapper">

                                    <Checkbox
                                        checked={checkList[indexOut][index]}
                                        onChange={() => self.picChecked(indexOut, index, item)}
                                        className="img-checkBox"
                                    >
                                    </Checkbox>

                                    <img className="monitor-img" key={index} src={item} onClick={() => { self.showView(text) }} />
                                </div>
                            })
                        }
                    </div>

                    // <img className="monitor-img" src={text[0]} onClick={() => { self.showView(text) }} />
                }
            }, {
                title: "含水分比-人工(%)",
                dataIndex: "waterRatioArtificial",
                key: "waterRatioArtificial",
                width: 100
            }, {
                title: "扣水分重(吨)",
                dataIndex: "buckleWaterWeight",
                key: "buckleWaterWeight",
                width: 100
            }, {
                title: "含杂质比(%)",
                dataIndex: "impurityRatio",
                key: "impurityRatio",
                width: 100
            }, {
                title: "扣杂质重(吨)",
                dataIndex: "impurityWeight",
                key: "impurityWeight",
                width: 100
            }, {
                title: "退货",
                dataIndex: "returnGoods",
                key: "returnGoods",
                width: 100
            }, {
                title: "退货原因",
                dataIndex: "returnGoodsReason",
                key: "returnGoodsReason",
                width: 200
            }, {
                title: "是否重检",
                dataIndex: "isReinspection",
                key: "isReinspection",
                width: 100,
                render: function (text) {
                    if (text == "Y") {
                        return "是"
                    } else {
                        return "否"
                    }
                }
            }, {
                title: "修改人姓名",
                dataIndex: "modifyuser",
                key: "modifyuser",
                width: 100,
            }, , {
                title: "重检原因",
                dataIndex: "modifierReason",
                key: "modifierReason",
                width: 150,
            }, {
                title: "叉车工工号",
                dataIndex: "forkliftWorkerNo",
                key: "forkliftWorkerNo",
                width: 100,
            }
        ]


        let title = this.onChangeHead(btnFlag);
        let {
            purchasing,
            planNo,
            serialNo,
            licensePlateNo,
            goodsName,
            materialVariety,
            auxiliarySupplier,
            totalPackageNo,
            columnNo,
            rowNo,
            layerNo,
            unpackNo,
            samplingLocation,
            pumpingPosition,
            poundNo,
            meterPoint,
            pk_meterPoint,
            weiCompleMark,
            grossWeight,
            tareWeight,
            netWeight,
            settlementWeight,
            singlePieceWeight,
            grossWeightTime,
            tareWeightTime,
            inspectedNo,
            heapNo,
            heapName,
            checkPoint,
            averageWaterRatio,
            averageDewateringWeight,
            averageImpurityRatio,
            averageimpurityWeight,
            priceCoefficient,
            averageUnitPrice,
            sumMoney,
            qualityGrade,
            pk_material,
            auditor,
            modifyuser,
            mainSupplier,
            pk_mainSupplier,
            department
        } = rowData;
        const { getFieldProps, getFieldError } = this.props.form;

        return (
            <div className='domesticPaper-detail'>
                <Loading
                    showBackDrop={true}
                    loadingType="line"
                    show={this.props.showLoading}
                />
                <Header title={title} back={true} backFn={this.onBack}>
                    {(btnFlag < 2) ? (
                        <div className='head-btn'>
                            <Button className='head-cancel' onClick={this.onBack}>取消</Button>
                            <Button className='head-save' onClick={this.save}>保存</Button>
                        </div>
                    ) : ''}
                </Header>
                {
                    self.showBpmComponent(btnFlag, appType ? appType : "1", id, processDefinitionId, processInstanceId, rowData)
                }
                <Row className='detail-body'>
                    <h4>基础信息</h4>
                    <Col md={4} xs={6}>
                        <Label>
                            采购组织：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('purchasing', {
                                validateTrigger: 'onBlur',
                                initialValue: purchasing || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('purchasing')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            计划单号：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('planNo', {
                                validateTrigger: 'onBlur',
                                initialValue: planNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('planNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            序列号：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('serialNo', {
                                validateTrigger: 'onBlur',
                                initialValue: serialNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('serialNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            车牌号：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('licensePlateNo', {
                                validateTrigger: 'onBlur',
                                initialValue: licensePlateNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('licensePlateNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            货物名称：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('goodsName', {
                                validateTrigger: 'onBlur',
                                initialValue: goodsName || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('goodsName')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            物料品种：
                                </Label>
                        <RefMultipleTableWithInput
                            disabled={btnFlag == 2 || true}
                            title={'物料'}

                            param={{
                                "id": "物料",

                            }}
                            refModelUrl={{
                                tableBodyUrl: '/jxzy/materialref/blobRefSearch',//表体请求
                                refInfo: '/jxzy/materialref/getRefModelInfo',//表头请求
                            }}
                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                            multiple={true}
                            searchable={true}
                            checkStrictly={true}
                            strictMode={true}

                            miniSearch={false}
                            displayField='{refname}'
                            valueField='refid'

                            {...getFieldProps('materialVarietyCode', {
                                initialValue: self.initValueString(materialVariety, pk_material),
                            })}
                        />

                        <span className='error'>
                            {getFieldError('materialVariety')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            主供应商：
                                </Label>
                        <RefMultipleTableWithInput
                            title={'主供应商'}
                            disabled={true}

                            param={{
                                "id": "主供应商",

                            }}
                            refModelUrl={{
                                tableBodyUrl: '/jxzy/suppref/blobRefSearch',//表体请求
                                refInfo: '/jxzy/suppref/getRefModelInfo',//表头请求
                            }}
                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                            multiple={false}
                            searchable={true}
                            checkStrictly={true}
                            strictMode={true}

                            miniSearch={false}
                            displayField='{refname}'
                            valueField='refid'

                            {...getFieldProps('mainSupplier', {
                                initialValue: self.initValueString(mainSupplier, pk_mainSupplier),

                            })}
                        />

                        <span className='error'>
                            {getFieldError('mainSupplier')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            辅供应商：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('auxiliarySupplier', {
                                validateTrigger: 'onBlur',
                                initialValue: auxiliarySupplier || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('auxiliarySupplier')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            总包数：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || false}
                            {
                            ...getFieldProps('totalPackageNo', {
                                validateTrigger: 'onBlur',
                                initialValue: totalPackageNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('totalBoxNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            列数：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('columnNo', {
                                validateTrigger: 'onBlur',
                                initialValue: columnNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('columnNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            排数：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('rowNo', {
                                validateTrigger: 'onBlur',
                                initialValue: rowNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('rowNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            层数：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('layerNo', {
                                validateTrigger: 'onBlur',
                                initialValue: layerNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('layerNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            开包件数：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || false}
                            {
                            ...getFieldProps('unpackNo', {
                                validateTrigger: 'onBlur',
                                initialValue: unpackNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('unpackNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            抽样位置：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('samplingLocation', {
                                validateTrigger: 'onBlur',
                                initialValue: samplingLocation || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('samplingLocation')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            加抽位置：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('pumpingPosition', {
                                validateTrigger: 'onBlur',
                                initialValue: pumpingPosition || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('pumpingPosition')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            审核人：
                                </Label>
                        <FormControl disabled={true}
                            {
                            ...getFieldProps('auditor', {
                                validateTrigger: 'onBlur',
                                initialValue: auditor || '',
                                rules: [{
                                    type: 'string', message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('auditor')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            修改人：
                                </Label>
                        <FormControl disabled={true}
                            {
                            ...getFieldProps('modifyuser', {
                                validateTrigger: 'onBlur',
                                initialValue: modifyuser || '',
                                rules: [{
                                    type: 'string', message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('modifyuser')}
                        </span>
                    </Col>
                    <Col md={12}><h4>称重信息</h4></Col>

                    <Col md={4} xs={6}>
                        <Label>
                            磅单号：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('poundNo', {
                                validateTrigger: 'onBlur',
                                initialValue: poundNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('poundNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            计量点：
                                </Label>
                        <RefMultipleTableWithInput
                            disabled={btnFlag == 2 || true}
                            title={'计量点'}

                            param={{
                                "id": "计量点",

                            }}
                            refModelUrl={{
                                tableBodyUrl: '/jxzy/meterpointref/blobRefSearch',//表体请求
                                refInfo: '/jxzy/meterpointref/getRefModelInfo',//表头请求
                            }}
                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                            multiple={false}
                            searchable={true}
                            checkStrictly={true}
                            strictMode={true}

                            miniSearch={false}
                            displayField='{refname}'
                            valueField='refid'

                            {...getFieldProps('meterPointcode', {
                                initialValue: self.initValueString(meterPoint, pk_meterPoint),
                            })}
                        />

                        <span className='error'>
                            {getFieldError('meterPoint')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            称重完成标记：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('weiCompleMark', {
                                validateTrigger: 'onBlur',
                                initialValue: weiCompleMark || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('weiCompleMark')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            毛重(吨)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('grossWeight', {
                                validateTrigger: 'onBlur',
                                initialValue: grossWeight || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('grossWeight')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            皮重(吨)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('tareWeight', {
                                validateTrigger: 'onBlur',
                                initialValue: tareWeight || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('tareWeight')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            净重(吨)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('netWeight', {
                                validateTrigger: 'onBlur',
                                initialValue: netWeight || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('netWeight')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            货重(吨)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('settlementWeight', {
                                validateTrigger: 'onBlur',
                                initialValue: settlementWeight || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('settlementWeight')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            单件重(吨)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('singlePieceWeight', {
                                validateTrigger: 'onBlur',
                                initialValue: singlePieceWeight || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('singlePieceWeight')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            称毛重时间：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('grossWeightTime', {
                                validateTrigger: 'onBlur',
                                initialValue: grossWeightTime || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('grossWeightTime')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            称皮重时间：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('tareWeightTime', {
                                validateTrigger: 'onBlur',
                                initialValue: tareWeightTime || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('tareWeightTime')}
                        </span>
                    </Col>
                    <Col md={12}><h4>检验信息</h4></Col>
                    <Col md={4} xs={6}>
                        <Label>
                            已检验人数：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('inspectedNo', {
                                validateTrigger: 'onBlur',
                                initialValue: inspectedNo || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('inspectedNo')}
                        </span>
                    </Col>

                    <Col md={4} xs={6}>
                        <Label>
                            堆号：
                                </Label>
                        <RefMultipleTableWithInput
                            disabled={btnFlag == 2 || false}
                            title={'堆号'}

                            param={{
                                "id": "堆号",

                            }}
                            refModelUrl={{
                                tableBodyUrl: '/jxzy/rackref/blobRefSearch',//表体请求
                                refInfo: '/jxzy/rackref/getRefModelInfo',//表头请求
                            }}
                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                            multiple={false}
                            searchable={true}
                            checkStrictly={true}
                            strictMode={true}

                            miniSearch={false}
                            displayField='{refname}'
                            valueField='refid'

                            {...getFieldProps('heapNocode', {
                                initialValue: self.initValueString(heapName, heapNo),
                            })}
                        />

                        <span className='error'>
                            {getFieldError('heapNo')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            检验点：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('checkPoint', {
                                validateTrigger: 'onBlur',
                                initialValue: checkPoint || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('checkPoint')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            平均含水分比(%)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('averageWaterRatio', {
                                validateTrigger: 'onBlur',
                                initialValue: averageWaterRatio || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('averageWaterRatio')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            平均扣水分重(吨)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('averageDewateringWeight', {
                                validateTrigger: 'onBlur',
                                initialValue: averageDewateringWeight || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('averageDewateringWeight')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            平均含杂质比(%)：
                                </Label>
                        <FormControl disabled={true}
                            {
                            ...getFieldProps('averageImpurityRatio', {
                                validateTrigger: 'onBlur',
                                initialValue: averageImpurityRatio || '',

                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('averageImpurityRatio')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            平均含杂质重(吨)：
                                </Label>
                        <FormControl disabled={true}
                            {
                            ...getFieldProps('averageimpurityWeight', {
                                validateTrigger: 'onBlur',
                                initialValue: averageimpurityWeight || '',
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('averageimpurityWeight')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            价格系数：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('priceCoefficient', {
                                validateTrigger: 'onBlur',
                                initialValue: priceCoefficient || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('priceCoefficient')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            平均单价(元)：
                                </Label>
                        <FormControl disabled={true}
                            {
                            ...getFieldProps('averageUnitPrice', {
                                validateTrigger: 'onBlur',
                                initialValue: averageUnitPrice || '',
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('averageUnitPrice')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            金额(元)：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('sumMoney', {
                                validateTrigger: 'onBlur',
                                initialValue: sumMoney || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('sumMoney')}
                        </span>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label>
                            质量等级：
                                </Label>
                        <FormControl disabled={btnFlag == 2 || true}
                            {
                            ...getFieldProps('qualityGrade', {
                                validateTrigger: 'onBlur',
                                initialValue: qualityGrade || '',
                                rules: [{
                                    type: 'string', required: true, pattern: /\S+/ig, message: '请输入',
                                }],
                            }
                            )}
                        />

                        <span className='error'>
                            {getFieldError('qualityGrade')}
                        </span>
                    </Col>
                </Row>
                <div className="title-wrapper">
                    <h4 className="table-title">检验明细</h4>
                    <Button className="table-btn" onClick={this.uploadPic} disabled={imgUrl.length > 0 ? false : true}>上传图片</Button>
                </div>
                <Table data={childList} columns={columnchild} />
                <Viewer
                    visible={visibleView}
                    onClose={() => { this.setState({ visibleView: false }); }}
                    images={imgList.map((li) => {
                        return {
                            src: `${li}`
                        }
                    })}
                />
            </div>
        )
    }
}

export default Form.createForm()(Edit);
