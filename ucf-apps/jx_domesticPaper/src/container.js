import React from 'react';
import mirror, { connect } from 'mirrorx';

// 组件引入
import DomesticPaperTable from './components/domesticPaper-root/DomesticPaperTable';
import DomesticPaperSelectTable from './components/domesticPaper-root/DomesticPaperSelectTable';
import DomesticPaperPaginationTable from './components/domesticPaper-root/DomesticPaperPaginationTable';
import DomesticPaperEdit from './components/domesticPaper-edit/Edit';
import DomesticPaperBpmChart from './components/domesticPaper-bpm-chart'
// 数据模型引入
import model from './model'
mirror.model(model);

// 数据和组件UI关联、绑定
export const ConnectedDomesticPaperTable = connect( state => state.domesticPaper, null )(DomesticPaperTable);
export const ConnectedDomesticPaperSelectTable = connect( state => state.domesticPaper, null )(DomesticPaperSelectTable);
export const ConnectedDomesticPaperPaginationTable = connect( state => state.domesticPaper, null )(DomesticPaperPaginationTable);
export const ConnectedDomesticPaperEdit = connect( state => state.domesticPaper, null )(DomesticPaperEdit);
export const ConnectedDomesticPaperBpmChart = connect( state => state.domesticPaper, null )(DomesticPaperBpmChart);
