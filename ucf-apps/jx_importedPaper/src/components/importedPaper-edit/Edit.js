import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { actions } from "mirrorx";
import queryString from 'query-string';
import {Select, Switch, Upload,Loading, Table, Button, Col, Row, InputGroup, FormControl, Checkbox, Modal, Panel, PanelGroup, Label, Message,Icon } from "tinper-bee";
import Radio from 'bee-radio';
// import { BpmTaskApprovalWrap } from 'yyuap-bpm';
import Header from "components/Header";
import DatePicker from 'bee-datepicker';
import Form from 'bee-form';
// import RefMultipleTableWithInput from 'pap-refer/lib/ref-multiple-table/src/index';
// import 'pap-refer/lib/ref-multiple-table/src/index.css';  
import {RefMultipleTableWithInput} from 'pap-refer/dist/index';
 import 'pap-refer/dist/index.css';
import DelModal from 'components/DelModal';
import moment from "moment";
import Viewer from 'react-viewer';
import 'react-viewer/dist/index.css';
import './edit.less';
import { setCookie, getCookie} from "utils";
import { func } from "prop-types";

const FormItem = Form.FormItem;
const format = "YYYY-MM-DD HH:mm:ss";


const renderContent = (value, row, index) => {
  const obj = {
    children: value,
    props: {},
  };
  if (index === 4) {
    obj.props.colSpan = 0;
  }
  return obj;
};

class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fileList: [{
                uid: -1,
                name: 'xxx.png',
                status: 'done',
                url: 'http://www.baidu.com/xxx.png',
            }],
            rowData: {},
            selectedQualify:"不合格",
            refHeapNoList: [],
            visibleView:false,
            imgList:[],
            refWharfList:[],
            fileNameData: props.rowData.attachment || [],//上传附件数据
        }
    }
    async componentWillMount() {
        await actions.importedPaper.getOrderTypes();
        let searchObj = queryString.parse(this.props.location.search);
        let { btnFlag } = searchObj;
        let reasonList,refHeapNoList,refWharfList;

        

        if (btnFlag && btnFlag > 0) {
            let { search_pk_pound } = searchObj;
            actions.importedPaper.queryDetail({ search_pk_pound });
            //let rowData = this.handleRefShow(tempRowData) || {};

            reasonList = await actions.importedPaper.getReasonList();
            // refHeapNoList = await actions.importedPaper.getRefHeapNo({id:"堆号"});
            // refWharfList = await actions.importedPaper.getRefrefWharf({id:"码头"});

            this.setState({
                reasonList:reasonList,
                // refHeapNoList:refHeapNoList,
                // refWharfList:refWharfList

            })
        }

        

    }
    save = () => {//保存
        let {rowData} = this.props;
        let {bodylist} = rowData;
        this.props.form.validateFields(async (err, values) => {
            //values.attachment = this.state.fileNameData;
            let numArray = [
            ];

            for(var item in values.bodylist) {
                let key = item.split('-')[1];
                let param = item.split('-')[0];
                bodylist[key][param] = values.bodylist[item];
            }
            
            values.bodylist = bodylist;
            for(let i=0,len=numArray.length; i<len; i++ ) {
                values[numArray[i]] = Number(values[numArray[i]]);
            }

            if(JSON.parse(values.heapNocode).refpk){
                values.heapName = JSON.parse(values.heapNocode).refname;
                values.heapNo = JSON.parse(values.heapNocode).refpk;
            }

            if(JSON.parse(values.wharfcode).refpk){
                values.pk_wharf = JSON.parse(values.wharfcode).refpk;
                values.wharf = JSON.parse(values.wharfcode).refname;
            }

            if(JSON.parse(values.materialVarietyCode).refpk){
                values.materialVariety = JSON.parse(values.materialVarietyCode).refname;
                values.pk_material = JSON.parse(values.materialVarietyCode).refpk;
            }

            if(JSON.parse(values.meterPointcode).refpk){
                values.pk_meterPoint = JSON.parse(values.meterPointcode).refpk;
                values.meterPoint = JSON.parse(values.meterPointcode).refname;
            }
            

            delete values.wharfcode;
            delete values.meterPointcode;
            delete values.heapNocode;
            delete values.materialVarietyCode;

            

            let saveObj = Object.assign({}, rowData, values);

            await actions.importedPaper.save(
                saveObj,
            );
        });
    }

    // // 处理参照回显
    // handleRefShow = (tempRowData) => {
    //     let rowData = {};
    //     if(tempRowData){

    //         let {
    //         } = tempRowData;

    //         this.setState({
    //         })
    //         rowData = Object.assign({},tempRowData,
    //             {
    //             }
    //         )
    //     }
    //     return rowData;
    // }

    onBack = async() => {
        window.history.go(-1);
    }

    // 动态显示标题
    onChangeHead = (btnFlag) => {
        let titleArr = ["新增","编辑","详情"];
        return titleArr[btnFlag]||'新增';
    }

    // 跳转到流程图
    onClickToBPM = (rowData) => {
        console.log("actions", actions);
        actions.routing.push({
            pathname: 'importedPaper-chart',
            search: `?id=${rowData.id}`
        })
    }

    showView = (data) => {
        this.setState({
            visibleView:true,
            imgList:data
        })
    }

    // 流程图相关回调函数
    onBpmStart = () => {
        actions.importedPaper.updateState({ showLoading: true });
    }
    onBpmEnd = () => {
        actions.importedPaper.updateState({ showLoading: false });
    }
    onBpmSuccess = () => {
          window.setTimeout(()=>{
                    actions.importedPaper.updateState({ showLoading: false });
                    // actions.routing.push('pagination-table');
                    actions.routing.goBack();
                },1000);
    }
    onBpmError = () => {
        actions.importedPaper.updateState({ showLoading: false });
    }

    // 审批面板展示
    showBpmComponent = (btnFlag, appType, id, processDefinitionId, processInstanceId, rowData) => {
        // btnFlag为2表示为详情
        if ((btnFlag == 2) && rowData && rowData['id']) {
            console.log("showBpmComponent", btnFlag)
            return (
                <div >
                    {/* {appType == 1 &&<BpmTaskApprovalWrap
                        id={rowData.id}
                        onBpmFlowClick={() => { this.onClickToBPM(rowData) }}
                        appType={appType}
                        onStart={this.onBpmStart}
                        onEnd={this.onBpmEnd}
                        onSuccess={this.onBpmSuccess}
                        onError={this.onBpmError}
                    />}
                    {appType == 2 &&<BpmTaskApprovalWrap
                        id={id}
                        processDefinitionId={processDefinitionId}
                        processInstanceId={processInstanceId}
                        onBpmFlowClick={() => { this.onClickToBPM(rowData) }}
                        appType={appType}
                        onStart={this.onBpmStart}
                        onEnd={this.onBpmEnd}
                        onSuccess={this.onBpmSuccess}
                        onError={this.onBpmError}
                    />} */}
                </div>

            );
        }
    }

    arryDeepClone = (array)=>{
        let result = [];
        if(array){
            array.map((item)=>{
                let temp = Object.assign([],item);
                result.push(temp);
            })
        }
    }

    renderBody = (btnFlag) => {
        let {getFieldError,getFieldProps} = this.props.form;
        let self = this;
        let {rowData={}} = this.props;
        let bodylist = rowData.bodylist;
        let {reasonList} = this.state;
        if(!bodylist) return <div></div>;
        let dom = [];
        bodylist.forEach(function(item,index) {
            dom.push(
                <div className="content">
                    <li>
                        <span>{index+1}</span>
                        <span>检查人员</span>
                        <span>
                            <FormControl disabled={true}
                                {
                                ...getFieldProps(`bodylist.packageNo-${index}`, {
                                    validateTrigger: 'onBlur',
                                    initialValue: item.packageNo || '',
                                    rules: [{
                                        type:'string', message: '请输入',
                                    }],
                                }
                                )}
                            />
                        </span>
                        <span>
                            <Select disabled={true}
                                disabled={true}
                                {
                                ...getFieldProps(`bodylist.reasonid-${index}`, {
                                    initialValue: item.reasonid,
                                    }
                                )}>
                                
                                {
                                    self.state.reasonList.map((item, index) => {
                                        return (
                                            <Select.Option key={index} value={item.refid}>{item.name}</Select.Option>
                                        )
                                    })
                                }
                            </Select>
                        </span>
                        <span>
                            <FormControl disabled={true}
                                {
                                ...getFieldProps(`bodylist.deductionRate-${index}`, {
                                    validateTrigger: 'onBlur',
                                    initialValue: item.deductionRate || '',
                                    rules: [{
                                        type:'string', message: '请输入',
                                    }],
                                }
                                )}
                            />
                        </span>
                    </li>
                    <li>
                        <span></span><span>核查人员</span>
                        <span>
                            <FormControl disabled={btnFlag == 2||false}
                                {
                                ...getFieldProps(`bodylist.audit_packageNo-${index}`, {
                                    validateTrigger: 'onBlur',
                                    initialValue: item.audit_packageNo || '',
                                    rules: [{
                                        type:'string', message: '请输入',
                                    }],
                                }
                                )}
                            />
                        </span>
                        <span>
                            <Select
                                disabled={btnFlag == 2||false}
                                {
                                ...getFieldProps(`bodylist.audit_reasonid-${index}`, {
                                    initialValue: item.audit_reasonid,
                                }
                                )}>
                                
                                {
                                    self.state.reasonList.map((item, index) => {
                                        return (
                                            <Select.Option key={index} value={item.refid}>{item.name}</Select.Option>
                                        )
                                    })
                                }
                            </Select>
                        </span>
                        <span>
                            <FormControl disabled={btnFlag == 2||false}
                                {
                                ...getFieldProps(`bodylist.audit_deductionRate-${index}`, {
                                    validateTrigger: 'onBlur',
                                    initialValue: item.audit_deductionRate || '',
                                    rules: [{
                                        type:'string', message: '请输入',
                                    }],
                                }
                                )}
                            />
                        </span>
                    </li>
                </div>
            )

            
        })

        return dom;
    }

    initValueString = (name,pk) => {
        let param = {
            refname:name,
            refpk:pk
        }

        return JSON.stringify(param);
    }

    handleChangePic = (info) => {
        let {rowData} = this.props;

        let fileList = info.fileList;
    
        //  自定义上传数量，只显示最近上传的文件   
        fileList = fileList.slice(-2);
    
        // 读取返回数据，并显示成文件链接形式
        fileList = fileList.map((file) => {
          if (file.response) {
            //组件会显示文件链接为超链接形式
            file.url = file.response.url;
          }
          return file;
        });
    
        // 通过从服务器返回的结果，过滤上传成功的文件
        fileList = fileList.filter((file) => {
          if (file.response) {
            return file.response.status === 'success';
          }
          return true;
        });
        // rowData.imgUrl.push(fileList);
        // this.setState({ rowData });
      }

    renderImg = () => {
        let {rowData} = this.props;
        let {imgUrl} = rowData;
        let imgDom = [];
        let {showDelIcon} = this.state;
        let self = this;
        imgUrl && imgUrl.map(function(item,index) {
        imgDom.push(<>
                    <img onClick={() => {self.showView(imgUrl)}} src={item} />
                    {showDelIcon &&<DelModal selectData={[{}]} modalTitle="删除图片"  modalContent="确认删除图片"  confirmFn={self.delImg(index,imgUrl)} >
                        <Icon type="uf-close"></Icon>
                    </DelModal>}
                    </>);
        })
        return <div className="edit-img-wraper">{imgDom}</div>
    }

    delImg = (index) => {
        let self = this;
        return function() {
            let {rowData} = self.props;
            rowData.imgUrl.splice(index,1);
            self.setState({rowData});
        }
        


    }

    delPic = () => {
        this.setState({showDelIcon:!this.state.showDelIcon});
    }

    // 通过search_id查询数据

    render() {
        const self = this;
        let {rowData} = this.props;
        let { btnFlag,appType, id, processDefinitionId, processInstanceId } = queryString.parse(this.props.location.search);
        btnFlag = Number(btnFlag);
        let {
            reasonList,
            visibleView,
            imgList,
            showDelIcon
            // refWharfList,
            // refHeapNoList
        } = this.state;
        // 将boolean类型数据转化为string
        // Object.keys(rowData).forEach(function(item){
        //     if(typeof rowData[item] === 'boolean'){
        //         rowData[item] = String(rowData[item]);
        //     }
        // });


        let title = this.onChangeHead(btnFlag);
        let { 
                purchasing,
                totalBoxNo,
                licensePlateNo,
                caseNo,
                weiCompleMark,
                planNo,
                poundNo,
                goodsName,
                materialVariety,
                supplyUnit,
                customsNo,
                totalEntryNo,
                realNo,
                totalPackageNo,
                meterPoint,
                estimatedTare,
                estimatedWeight,
                grossWeight,
                tareWeight,
                netWeight,
                shortWeight,
                singlePieceWeight,
                grossWeightTime,
                tareWeightTime,
                inspector,
                wharf,
                heapNo,
                heapName,
                pk_meterPoint,
                sampleNo,
                unpackNo,
                radiationValue,
                inspectTime,
                memo,
                isQualify,
                packageNo,
                reason,
                deductionRate,
                imgUrl,
                bodylist,
                pk_wharf,
                pk_material,
                calPieceNumber,
                totcalPieceNumber,
                unboxWeightPerPiece,
                cBDehydrantWeight,
                cBDecontaminationWeight,
                cBUnqualifiedPaperWeight,
                auditor,
                settlementWeight,
                modifyuser
             } = rowData || {};

        const { getFieldProps, getFieldError } = this.props.form;
        
        
        return (
            <div className='importedPaper-detail'>
                <Loading
                    showBackDrop={true}
                    loadingType="line"
                    show={this.props.showLoading}
                />
                <Header title={title} back={true} backFn={this.onBack}>
                    {(btnFlag < 2) ? (
                        <div className='head-btn'>
                            <Button className='head-cancel' onClick={this.onBack}>取消</Button>
                            <Button className='head-save' onClick={this.save}>保存</Button>
                        </div>
                    ) : ''}
                </Header>
                {
                    self.showBpmComponent(btnFlag, appType ? appType : "1", id, processDefinitionId, processInstanceId, rowData)
                }
                <Row className='detail-body'>
                            <h4>基础信息</h4>
                            <Col md={4} xs={6}>
                                <Label>
                                    采购组织：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('purchasing', {
                                            validateTrigger: 'onBlur',
                                            initialValue: purchasing || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('purchasing')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    计划单号：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('planNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: planNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('planNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    总箱数：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('totalBoxNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: totalBoxNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('totalBoxNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    供货单位：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('supplyUnit', {
                                            validateTrigger: 'onBlur',
                                            initialValue: supplyUnit || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('supplyUnit')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    货物名称：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('goodsName', {
                                            validateTrigger: 'onBlur',
                                            initialValue: goodsName || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('goodsName')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    物料品种：
                                </Label>
                                    <RefMultipleTableWithInput
                                        disabled = {true}
                                        title={'物料'}

                                        param={{
                                            "id": "物料",

                                        }}
                                        refModelUrl={{
                                            tableBodyUrl: '/jxzy/materialref/blobRefSearch',//表体请求
                                            refInfo: '/jxzy/materialref/getRefModelInfo',//表头请求
                                        }}
                                        matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                        filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                        multiple={true}
                                        searchable={true}
                                        checkStrictly= {true}
                                        strictMode = {true}
                                        
                                        miniSearch={false}
                                        displayField='{refname}'
                                        valueField='refid'

                                        {...getFieldProps('materialVarietyCode', {
                                            initialValue: self.initValueString(materialVariety,pk_material),
                                        })}
                                    />

                                <span className='error'>
                                    {getFieldError('materialVariety')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    箱号：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('caseNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: caseNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('caseNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    车牌号：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('licensePlateNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: licensePlateNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('licensePlateNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    海关单号：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('customsNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: customsNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('customsNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    总来数(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('totalEntryNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: totalEntryNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('totalEntryNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    实来数(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('realNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: realNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('realNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    总件数：
                                </Label>
                                    <FormControl disabled={btnFlag == 2 || false}
                                        {
                                        ...getFieldProps('totalPackageNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: totalPackageNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('totalPackageNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    审核人：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('auditor', {
                                            validateTrigger: 'onBlur',
                                            initialValue: auditor || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('auditor')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    修改人：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('modifyuser', {
                                            validateTrigger: 'onBlur',
                                            initialValue: modifyuser || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('modifyuser')}
                                </span>
                            </Col>
                            <Col md={12}><h4>称重信息</h4></Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    磅单号：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('poundNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: poundNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('poundNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    计量点：
                                </Label>
                                    <RefMultipleTableWithInput
                                         disabled = {true}
                                        title={'计量点'}

                                        param={{
                                            "id": "计量点",

                                        }}
                                        refModelUrl={{
                                            tableBodyUrl: '/jxzy/meterpointref/blobRefSearch',//表体请求
                                            refInfo: '/jxzy/meterpointref/getRefModelInfo',//表头请求
                                        }}
                                        matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                        filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                        multiple={false}
                                        searchable={true}
                                        checkStrictly= {true}
                                        strictMode = {true}
                                        
                                        miniSearch={false}
                                        displayField='{refname}'
                                        valueField='refid'

                                        {...getFieldProps('meterPointcode', {
                                            initialValue: self.initValueString(meterPoint,pk_meterPoint),
                                        })}
                                    />

                                <span className='error'>
                                    {getFieldError('meterPoint')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    称重完成标记：
                                </Label>

                                    <Select disabled={true}
                                        className="audit-select"
                                        {
                                                ...getFieldProps('weiCompleMark', {
                                                initialValue: weiCompleMark || '',
                                            })
                                        }
                                        >
                                                <Select.Option value="">请选择</Select.Option>
                                                <Select.Option value="1">未完成状态</Select.Option>
                                                <Select.Option value="2">完成状态</Select.Option>
                                                <Select.Option value="3">修改待审核</Select.Option>
                                                <Select.Option value="4">作废待审核</Select.Option>
                                                <Select.Option value="5">作废</Select.Option>
                                                <Select.Option value="6">待回皮</Select.Option>
                                                    
                                    </Select>

                                <span className='error'>
                                    {getFieldError('weiCompleMark')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    估算皮重(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('estimatedTare', {
                                            validateTrigger: 'onBlur',
                                            initialValue: estimatedTare || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('estimatedTare')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    估算重量(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('estimatedWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: estimatedWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('estimatedWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    毛重(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('grossWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: grossWeight || '',
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('grossWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    皮重(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('tareWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: tareWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('tareWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    净重(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('netWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: netWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('netWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    短重：
                                </Label>
                                <Select disabled={true}
                                    className="audit-select"
                                    {
                                            ...getFieldProps('shortWeight', {
                                            initialValue: shortWeight || '',
                                        })
                                    }
                                    >
                                            <Select.Option value="~">否</Select.Option>
                                            <Select.Option value="Y">是</Select.Option>
                                            <Select.Option value="N">否</Select.Option>
                                                
                                </Select>
                                <span className='error'>
                                    {getFieldError('shortWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    单件重(吨)：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('singlePieceWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: singlePieceWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('singlePieceWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    称毛重时间：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('grossWeightTime', {
                                            validateTrigger: 'onBlur',
                                            initialValue: grossWeightTime || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('grossWeightTime')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    称皮重时间：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('tareWeightTime', {
                                            validateTrigger: 'onBlur',
                                            initialValue: tareWeightTime || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('tareWeightTime')}
                                </span>
                            </Col>
                             <Col md={4} xs={6}>
                                <Label>
                                    货重：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('settlementWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: settlementWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('tareWeightTime')}
                                </span>
                            </Col>
                            <Col md={12}><h4>检验信息</h4></Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    检验员：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('inspector', {
                                            validateTrigger: 'onBlur',
                                            initialValue: inspector || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('inspector')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    码头：
                                </Label>
                                <RefMultipleTableWithInput
                                    disabled = {true}
                                    title={'码头'}

                                    param={{
                                        "id": "码头",

                                    }}
                                    refModelUrl={{
                                        tableBodyUrl: '/jxzy/bdref/blobRefSearch',//表体请求
                                        refInfo: '/jxzy/bdref/getRefModelInfo',//表头请求
                                    }}
                                    matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                    filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                    multiple={false}
                                    searchable={true}
                                    checkStrictly= {true}
                                    strictMode = {true}
                                    
                                    miniSearch={false}
                                    displayField='{refname}'
                                    valueField='refid'

                                    {...getFieldProps('wharfcode', {
                                        initialValue: self.initValueString(wharf,pk_wharf),
                                    })}
                                />

                                <span className='error'>
                                    {getFieldError('pk_wharf')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    堆号：
                                </Label>
                                   
                                <RefMultipleTableWithInput
                                    disabled={btnFlag == 2||false}
                                    title={'堆号'}

                                    param={{
                                        "id": "堆号",

                                    }}
                                    refModelUrl={{
                                        tableBodyUrl: '/jxzy/rackref/blobRefSearch',//表体请求
                                        refInfo: '/jxzy/rackref/getRefModelInfo',//表头请求
                                    }}
                                    matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                    filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                    multiple={false}
                                    searchable={true}
                                    checkStrictly= {true}
                                    strictMode = {true}
                                    
                                    miniSearch={false}
                                    displayField='{refname}'
                                    valueField='refid'

                                    {...getFieldProps('heapNocode', {
                                        initialValue: self.initValueString(heapName,heapNo),
                                    })}
                                />
                                <span className='error'>
                                    {getFieldError('heapNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    样本号：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('sampleNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: sampleNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('sampleNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    开包件数：
                                </Label>
                                    <FormControl disabled={btnFlag == 2 || false}
                                        {
                                        ...getFieldProps('unpackNo', {
                                            validateTrigger: 'onBlur',
                                            initialValue: unpackNo || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('unpackNo')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    辐射值：
                                </Label>
                                    <FormControl disabled={btnFlag == 2||false}
                                        {
                                        ...getFieldProps('radiationValue', {
                                            validateTrigger: 'onBlur',
                                            initialValue: radiationValue || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('radiationValue')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    检验时间：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('inspectTime', {
                                            validateTrigger: 'onBlur',
                                            initialValue: inspectTime || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('inspectTime')}
                                </span>
                            </Col>
                            
                            
                            <Col md={4} xs={6}>
                                <Label>
                                    计算件数：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('calPieceNumber', {
                                            validateTrigger: 'onBlur',
                                            initialValue: calPieceNumber || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('calPieceNumber')}
                                </span>
                            </Col>
                            
                            <Col md={4} xs={6}>
                                <Label>
                                    是否合格：
                                </Label>
                                    <div style={{ marginTop: 0 }}>
                                      <Radio.RadioGroup 
                                        disabled={btnFlag == 2 || false}
                                        //selectedValue={this.state.selectedQualify}
                                        //selectedValue={"不合格"}
                                        {
                                        ...getFieldProps('isQualify', {
                                            initialValue: isQualify || '合格' ,
                                            }
                                        )}
                                        >
                                        <Radio.RadioButton disabled={btnFlag == 2 || false} value="合格">合格</Radio.RadioButton>
                                        <Radio.RadioButton disabled={btnFlag == 2 || false} value="不合格">不合格</Radio.RadioButton>
                                      </Radio.RadioGroup>
                                    </div>

                                <span className='error'>
                                    {getFieldError('isQualify')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                    总计算件数：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('totcalPieceNumber', {
                                            validateTrigger: 'onBlur',
                                            initialValue: totcalPieceNumber || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('totcalPieceNumber')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                开包每件重：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('unboxWeightPerPiece', {
                                            validateTrigger: 'onBlur',
                                            initialValue: unboxWeightPerPiece || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('unboxWeightPerPiece')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                本箱扣水分重：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('cBDehydrantWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: cBDehydrantWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('cBDehydrantWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                本箱杂质扣重：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('cBDecontaminationWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: cBDecontaminationWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('cBDecontaminationWeight')}
                                </span>
                            </Col>
                            <Col md={4} xs={6}>
                                <Label>
                                本箱不合格纸重：
                                </Label>
                                    <FormControl disabled={true}
                                        {
                                        ...getFieldProps('cBUnqualifiedPaperWeight', {
                                            validateTrigger: 'onBlur',
                                            initialValue: cBUnqualifiedPaperWeight || '',
                                            rules: [{
                                                type:'string', message: '请输入',
                                            }],
                                        }
                                        )}
                                    />

                                <span className='error'>
                                    {getFieldError('cBUnqualifiedPaperWeight')}
                                </span>
                            </Col>
                            
                            <Col md={12}><h4>检验列表信息</h4></Col>
                            <Col md={12}>
                            <div>
                                <ul className="bodylist-ul">
                                    <li><span>序号</span><span>人员</span><span>件数</span><span>原因</span><span>扣率</span></li>
                                    {this.renderBody(btnFlag)}
                                </ul>
                            </div>
                            </Col>
                            <Col md={12}><h4>检验照片</h4></Col>
                            <Col md={12}>
                                {/* <Upload
                                    action = '/upload.do'
                                    onChange = {this.handleChangePic}
                                    multiple= {true}
                                    >
                                    <Button colors="primary" shape="border">
                                    <Icon type="uf-upload" /> 添加照片
                                    </Button>
                                </Upload> */}
                                {!showDelIcon &&<Button colors="primary" shape="border" onClick={this.delPic}>删除照片</Button>}
                                {showDelIcon && <Button colors="primary" shape="border" onClick={this.delPic}>取消删除照片</Button>}
                            </Col>
                            
                            {this.renderImg()}
                            <Viewer
                                visible={visibleView}
                                onClose={() => { this.setState({ visibleView: false }); } }
                                images={imgList.map((li) => {
                                    return {
                                        src: `${li}`
                                    }
                                }) }
                            />
                            
                </Row>


            </div>
        )
    }
}

export default Form.createForm()(Edit);
