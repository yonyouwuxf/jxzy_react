import React, { Component } from 'react'
import { actions } from "mirrorx";
import { Switch, InputNumber, Col, Row,FormControl, Label, Select,Radio,Form } from "tinper-bee";
import SearchPanel from 'components/SearchPanel';
// import RefMultipleTableWithInput from 'pap-refer/lib/ref-multiple-table/src/index';
// import 'pap-refer/lib/ref-multiple-table/src/index.css';  
import {RefMultipleTableWithInput} from 'pap-refer/dist/index';
 import 'pap-refer/dist/index.css';
import moment from 'moment';
import DatePicker from "tinper-bee/lib/Datepicker";
import { deepClone } from "utils";
const { RangePicker } = DatePicker;
const FormItem = Form.FormItem;
import './index.less'

class ImportedPaperForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            pk_importedPaper: '',
        }
    }
    componentWillMount(){
        // 获得进口废纸称重列表数据
        actions.importedPaper.getOrderTypes();
    }
    /** 查询数据
     * @param {*} error 校验是否成功
     * @param {*} values 表单数据
     */
    search = (error,values) => {
        const {searchValuesCall} = this.props;
        this.props.form.validateFields(async (err, values) => {
            if(JSON.parse(values.wharfcode).refpk){
                values.pk_wharf = JSON.parse(values.wharfcode).refpk;
                values.wharf = JSON.parse(values.wharfcode).refname;
            }

            if(JSON.parse(values.heapNocode).refpk){
                values.heapNo = JSON.parse(values.heapNocode).refpk;
                values.heapName = JSON.parse(values.heapNocode).refname;
            }

            delete values.heapNocode;

            if(values.billDate[0]) {
                values.startBillDate = values.billDate[0] && values.billDate[0].format("YYYY-MM-DD") + ' 00:00:00';
                values.endBillDate = values.billDate[1] && values.billDate[1].format("YYYY-MM-DD") + ' 23:59:59';
            }

            delete values.billDate;

            if(JSON.parse(values.materialVarietyCode).refpk){
                values.pk_material = JSON.parse(values.materialVarietyCode).refpk;
            }

            if(values.orgCode && JSON.parse(values.orgCode).refpk){
                values.orgNo = JSON.parse(values.orgCode).refpk;
                //values.orgName = JSON.parse(values.materialVarietyCode).refName;
            }
            delete values.orgCode;

            delete values.wharfcode;
            delete values.materialVarietyCode;
            for(let attr in values){
                if(!values[attr])
                    delete values[attr];
            }
            this.getQuery(values, 0);
        });
    }

    getQuery = (values, type) => {
        let queryParam = deepClone(this.props.queryParam);
        let {pageParams, whereParams} = queryParam;
        queryParam.whereParams = values;
        queryParam.pageParams = {
            pageIndex: 0,
            pageSize: 10,
        };
        actions.importedPaper.updateState(queryParam);
        if (type === 0) {
            actions.importedPaper.loadList(queryParam);
        }
    }

    initValueString = (name,pk) => {
        let param = {
            refname:name,
            refpk:pk
        }

        return JSON.stringify(param);
    }
    
    /**
     * 重置
     */
    reset = () => {
        let {queryParam} = this.props;
        queryParam.whereParams = {
            pound:"",
            poundNo:"",
            caseNo:"",
            sampleNo:"",
            materialVariety:"",
            wharf:"",
            pk_wharf:"",
            pk_material:"",
            startBillDate:"",
            endBillDate:"",
            weiCompleMark:"",
            auditStatus:"",
            licensePlateNo:"",
            heapNo:"",
            heapName:"",
            isQualify:"",
            orgCode:"",
            orgNo:"",
            orgName:"",
            totalEntryNo:""
        };
        actions.importedPaper.updateState(queryParam);
    }

    render(){
        const { getFieldProps, getFieldError } = this.props.form;
        let { orderTypes,searchValues,queryParam } = this.props;
        let self = this;
        let {
            pound="",
            poundNo="",
            caseNo="",
            sampleNo="",
            materialVariety="",
            wharf="",
            pk_wharf="",
            pk_material="",
            startBillDate,
            endBillDate,
            weiCompleMark="",
            auditStatus="",
            licensePlateNo="",
            heapNo="",
            heapName="",
            isQualify,
            orgName,
            orgNo,
            totalEntryNo
        } = queryParam.whereParams || {};
        return (
            <SearchPanel
                className='search-form'
                form={this.props.form}
                reset={this.reset}
                search={this.search}>
                    <div className='demo-body'>
                        <Form>
                            <Row>
                            <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>采购组织</Label>
                                        <RefMultipleTableWithInput
                                            title={'采购组织'}

                                            param={{
                                                "id": "采购组织",
                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/OrgRef/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/OrgRef/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('orgCode', {
                                                initialValue: self.initValueString(orgName,orgNo),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单号</Label>
                                        <FormControl
                                                {...getFieldProps('poundNo', {
                                                    initialValue: poundNo,
                                                })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>堆号</Label>
                                        <RefMultipleTableWithInput
                                            title={'堆号'}

                                            param={{
                                                "id": "堆号",

                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/rackref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/rackref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('heapNocode', {
                                                initialValue: self.initValueString(heapName,heapNo),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>箱号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('caseNo', {
                                                    initialValue: caseNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>样本号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('sampleNo', {
                                                    initialValue: sampleNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>物料品种</Label>
                                        <RefMultipleTableWithInput
                                            title={'物料'}

                                            param={{
                                                "id": "物料",
                                                "sysId":"type2"
                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/materialref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/materialref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('materialVarietyCode', {
                                                initialValue: self.initValueString(materialVariety,pk_material),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>车牌号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('licensePlateNo', {
                                                    initialValue: licensePlateNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>码头</Label>
                                        <RefMultipleTableWithInput
                                            title={'码头'}

                                            param={{
                                                "id": "码头",

                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/bdref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/bdref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={false}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('wharfcode', {
                                                initialValue: self.initValueString(wharf,pk_wharf),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>称重完成标记</Label>
                                       

                                            <Select
                                                className="audit-select"
                                                {
                                                        ...getFieldProps('weiCompleMark', {
                                                        initialValue: weiCompleMark || "",
                                                    })
                                                }
                                                >
                                                        <Select.Option value="">请选择</Select.Option>
                                                        <Select.Option value="1">未完成状态</Select.Option>
                                                        <Select.Option value="2">完成状态</Select.Option>
                                                        <Select.Option value="3">修改待审核</Select.Option>
                                                        <Select.Option value="4">作废待审核</Select.Option>
                                                        <Select.Option value="5">作废</Select.Option>
                                                        <Select.Option value="6">待回皮</Select.Option>
                                                            
                                                </Select>



                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>审核状态</Label>
                                        <Select
                                        className="audit-select"
                                        {
                                                ...getFieldProps('auditStatus', {
                                                initialValue: auditStatus || "Y",
                                            })
                                        }
                                        >
                                                <Select.Option value="">全部</Select.Option>
                                                <Select.Option value="Y">已审核</Select.Option>
                                                <Select.Option value="N">未审核</Select.Option>
                                                    
                                        </Select>
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单日期</Label>
                                        <RangePicker
                                            format="YYYY-MM-DD"
                                            // locale={locale}
                                            placeholder={'开始 ~ 结束'}
                                            showClear={true}
                                            {...getFieldProps('billDate', {
                                                initialValue: startBillDate && [moment(startBillDate),moment(endBillDate)] || [moment(),moment()],
                                                validateTrigger: 'onBlur'
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>是否合格</Label>
                                        <Radio.RadioGroup 
                                            {
                                            ...getFieldProps('isQualify', {
                                                initialValue: isQualify || "",
                                                }
                                            )}
                                            >
                                            <Radio.RadioButton value="合格">合格</Radio.RadioButton>
                                            <Radio.RadioButton value="不合格">不合格</Radio.RadioButton>
                                        </Radio.RadioGroup>
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>总来数</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('totalEntryNo', {
                                                    initialValue: totalEntryNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                            </Row>
                        </Form>
                    </div>
            </SearchPanel>
        )
    }
}

export default Form.createForm()(ImportedPaperForm)