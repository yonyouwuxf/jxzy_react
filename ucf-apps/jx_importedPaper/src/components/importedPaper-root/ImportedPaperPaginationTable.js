import React, { Component } from 'react'
import PaginationTable from 'components/PaginationTable'
// import {BpmButtonSubmit,BpmButtonRecall} from 'yyuap-bpm';
import { actions } from 'mirrorx';
import { Button,Message,Modal, Loading,Tooltip } from 'tinper-bee';
import Select from 'bee-select';
import moment from "moment/moment";
import Header from 'components/Header';
import DelModal from 'components/DelModal';
import ImportedPaperForm from '../importedPaper-form';
import ShortWeightRangeModal from '../custom-modal/ShortWeightRange.js';
import AcExport from '../importedPaper-export';
import ExportImg from '../custom-modal/ExportImg.js';
import { deepClone } from "utils";
import './index.less'
export default class ImportedPaperPaginationTable extends Component {
    constructor(props){
        super(props);
        let self=this;
        this.state = {
            // 表格中所选中的数据，拿到后可以去进行增删改查
            step: 10,
            showModal:false,
            delData:[],
            selectData:[],
            searchValues:{},
            column:[
                {
                    title: "进口明细单号",
                    dataIndex: "applicationNo",
                    key: "applicationNo",
                    width:120
                },{
                    title: "车牌号",
                    dataIndex: "licensePlateNo",
                    key: "licensePlateNo",
                    width:80
                },{
                    title: "箱号",
                    dataIndex: "caseNo",
                    key: "caseNo",
                    width:100
                },
                {
                    title: "总来数",
                    dataIndex: "totalEntryNo",
                    key: "totalEntryNo",
                    width:80
                },{
                    title: "实来数",
                    dataIndex: "realNo",
                    key: "realNo",
                    width:80
                },{
                    title: "毛重",
                    dataIndex: "grossWeight",
                    key: "grossWeight",
                    width:80,
                },{
                    title: "皮重",
                    dataIndex: "tareWeight",
                    key: "tareWeight",
                    width:80,
                },{
                    title: "货重",
                    dataIndex: "settlementWeight",
                    key: "settlementWeight",
                    width:80,
                },{
                    title: "扣重",
                    dataIndex: "deductWeight",
                    key: "deductWeight",
                    width:80,
                },{
                    title: "总件重",
                    dataIndex: "totalPackageNo",
                    key: "totalPackageNo",
                    width:80,
                },{
                    title: "堆号",
                    dataIndex: "heapName",
                    key: "heapName",
                    width:120
                },{
                    title: "磅单号",
                    dataIndex: "poundNo",
                    key: "poundNo",
                    width:120
                },{
                    title: "审核状态",
                    dataIndex: "auditStatus",
                    key: "auditStatus",
                    width:80,
                    render(text, record, index) {
                        if(text == "Y") {
                            return "审核完成"
                        }else {
                            return "未审核"
                        }
                    }
                },{
                    title: "供应商",
                    dataIndex: "supplyUnit",
                    key: "supplyUnit",
                    fixed: "left",
                    width:150,
                    render: (text, record, index) => {
                        return (
                            <Tooltip inverse overlay={text}>
                                <span tootip={text} className="popTip">{text}</span>
                            </Tooltip>
                        );
                    }
                },{
                    title: "物料品种",
                    dataIndex: "materialVariety",
                    key: "materialVariety",
                    fixed: "left",
                    width:200,
                    render: (text, record, index) => {
                        return (
                            <Tooltip inverse overlay={text}>
                                <span tootip={text} className="popTip">{text}</span>
                            </Tooltip>
                        );
                    }
                },{
                    title: "操作",
                    dataIndex: "d",
                    key: "d",
                    width:100,
                    fixed: "right",
                    render(text, record, index) {
                        return (
                            <div className='operation-btn'>
                                <i size='sm' className='uf uf-search edit-btn' onClick={() => { self.cellClick(record,2) }}></i>
                                {record.auditStatus != 'Y' && <i size='sm' className='uf uf-pencil edit-btn' onClick={() => { self.cellClick(record,1) }}></i>}
                            </div>
                        )
                    }
                }
            ]
        }
    }
    
    componentDidMount(){
        actions.importedPaper.loadList(this.props.queryParam);
        actions.importedPaper.getAuth("002");
    }

    /**
     * 编辑,详情，增加
     */

    cellClick = async (record,btnFlag) => {
        let {searchValues} = this.state;
        await actions.importedPaper.updateState({
            rowData : record,
        });

        sessionStorage.setItem("searchValues",JSON.stringify(searchValues));


        let pk_pound = "";
        if(record){
            pk_pound = record["pk_pound"];
        }
        actions.routing.push(
            {
                pathname: 'importedPaper-edit',
                search:`?search_pk_pound=${pk_pound}&btnFlag=${btnFlag}`
            }
        )
    }

    // 删除操作
    delItem = (record, index) => {
        this.setState({
            showModal:true,
            delData:[{ id: record.id,ts: record.ts }]
        });

    }

    // 表格勾选回调函数，返回选中数据
    onTableSelectedData = data => {
        this.setState({
            selectData: data
        })
    }
    // 分页单页数据条数选择函数
    onPageSizeSelect = (index, value) => {
        let queryParam = deepClone(this.props.queryParam);
        let {pageIndex, pageSize} = queryParam.pageParams;
        pageSize = value;
        queryParam['pageParams'] = { pageIndex, pageSize };
        actions.importedPaper.loadList(queryParam);
    }

    // 分页组件点击页面数字索引执行函数
    onPageIndexSelect = (value, type) => {
        let queryParam = deepClone(this.props.queryParam);
        let { pageIndex, pageSize } = queryParam.pageParams;
        pageIndex = value - 1;

        //let { pageIndex, pageSize } = getPageParam(value, type, queryParam.pageParams);
        queryParam['pageParams'] = { pageIndex, pageSize };
        this.setState({ selectedIndex: 0 }); //默认选中第一行
        actions.importedPaper.loadList(queryParam);
    }

    // 模态框确认删除
    onModalDel = async (delFlag)=>{
        let {delData} = this.state;
        if(delFlag){
            await actions.importedPaper.delItem({
                param: delData
            });
        }
        this.setState({
            showModal:false,
            delData:[]
        })
    }
    // 模板下载
    onLoadTemplate = () => {
        window.open(`${GROBAL_HTTP_CTX}/jx_importedPaper/excelTemplateDownload`)
    }

    // 导入成功回调函数
    handlerUploadSuccess = (data)=>{
        // 导入成功后，列表加载数据
        Message.create({content: '导入数据成功', color: 'success'});
        actions.importedPaper.loadList();
    }

    // 导入删除回调函数
    handlerUploadDelete = (file) => {

    }

    // 导出
    exportExcel = () =>{
        //actions.importedPaper.exportExcel(this.queryParams);
        let  {selectData} = this.state;
        if(selectData.length > 0) {
            actions.importedPaper.exportExcel({
                dataList : selectData
            });
        }else {
            Message.create({ content: '请选择导出数据', color : 'danger'  });
        }
    }

    // 打印数据
    printExcel = ()=>{
        let {selectData} = this.state;
        if(!selectData.length)  
        {
            Message.create({ content: '请选择需打印的数据', color : 'danger'  });
            return;
        }
        actions.importedPaper.printExcel({
            queryParams:
            {
                funccode: 'importedPaper',
                nodekey: '002'
            },
            printParams: 
            {
                id:selectData[0].id
            }
        });
    }

    // 动态设置列表滚动条x坐标
    getCloumnsScroll = (columns) => {
        let sum = 0;
        columns.forEach((da) => {
            sum += da.width;
        })
        return (sum);
    }

    onConfirmVerify = () => {
        let {selectData} = this.state;
        actions.importedPaper.Verify({
            param: selectData
        });
        actions.importedPaper.loadList(this.props.queryParam);
        this.setState({selectData:[]});
    }

    cancelSelect = () => {
        this.setState({selectData:[]});
    }

    onConfirmCancelVerify = () => {
        let {selectData} = this.state;
        actions.importedPaper.CancelVerify({
            param: selectData
        });
        actions.importedPaper.loadList(this.props.queryParam);
        this.setState({selectData:[]})
    }

    exportValuesCall = () => {
        let {queryParam} = this.props;
        actions.importedPaper.loadList(queryParam);
    }

    render(){
        const self=this;
        let { list, showLoading, pageIndex, pageSize, totalPages , total,authFlag,shortWeightAuth,queryParam } = this.props;
        let {showModal,searchValues,selectData} = this.state;
        let exportProps = { total, pageIndex, pageSize, selectData, list,searchValues,queryParam};
        // 将boolean类型数据转化为string
        list.forEach(function(item){
            Object.keys(item).forEach(function(key){
                if(typeof item[key] === 'boolean'){
                    item[key] = String(item[key]);
                }
             })
         })

        return (
            <div className='importedPaper-root'>
                <ImportedPaperForm { ...this.props }/>
                <div className='table-header'>
                    <DelModal operType={'audittrue'} batch={true} selectData={selectData} modalTitle="审核"  modalContent="确认审核通过"  confirmFn={this.onConfirmVerify} >
                        <Button colors="primary" style={{"marginLeft":15}} size='sm' >
                        审核
                        </Button>
                    </DelModal>

                    <DelModal operType={'auditfalse'} batch={true} selectData={selectData} modalTitle="取消审核"  modalContent="确认取消审核通过"  confirmFn={this.onConfirmCancelVerify} >
                        <Button colors="primary" style={{"marginLeft":15}} size='sm' >
                        取消审核
                        </Button>
                    </DelModal>
                    {shortWeightAuth && <ShortWeightRangeModal cancelSelect={this.cancelSelect} />}
                    <AcExport exportValuesCall={this.exportValuesCall} {...exportProps} className="ml5"/>
                    <ExportImg />
                </div>
                <PaginationTable
                        data={list}
                        rowClassName={(record,index,indent)=>{
                            if (record.auditStatus == 'Y') {
                                return 'selected';
                            } else {
                                return 'aa';
                            }
                          }}
                        pageIndex={pageIndex}
                        pageSize={pageSize}
                        totalPages={totalPages}
                        total = {total}
                        columns={this.state.column}
                        checkMinSize={6}
                        onTableSelectedData={this.onTableSelectedData}
                        onPageSizeSelect={this.onPageSizeSelect}
                        onPageIndexSelect={this.onPageIndexSelect}
                />
                <Loading show={showLoading} loadingType="line" />
                <Modal
                        show={showModal}
                        onHide={this.close} >
                    <Modal.Header>
                        <Modal.Title>确认删除</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        是否删除选中内容
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={()=>this.onModalDel(false)} shape="border" style={{ marginRight: 50 }}>关闭</Button>
                        <Button onClick={()=>this.onModalDel(true)} colors="primary">确认</Button>
                    </Modal.Footer>
                </Modal>
            </div>

        )

    }
}