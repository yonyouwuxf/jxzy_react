import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { actions } from "mirrorx";

import Header from 'components/Header';
import ImportedPaperTable from '../importedPaper-table';
import ImportedPaperForm from '../importedPaper-form';

import './index.less';

/**
 * ImportedPaperRoot Component
 */
class ImportedPaperRoot  extends Component {
    constructor(props) {
        super(props);
        this.state = { }
    }
    /**
     *
     */
    componentWillMount() {
        this.getTableData();
    }
    /**
     * 获取table表格数据
     */
    getTableData = () => {
        actions.importedPaper.loadList();
    }

    render() {
        let { pageSize, pageIndex, totalPages} = this.props;
        return (
            <div className='importedPaper-root'>
                <Header title='进口废纸称重' back={true}/>
                <ImportedPaperForm { ...this.props }/>
                <ImportedPaperTable { ...this.props }/>
            </div>
        )
    }
}
export default ImportedPaperRoot;