import request from "utils/request";
import axios from "axios";
//定义接口地址
const URL = {
    "GET_DETAIL":  `${GROBAL_HTTP_CTX}/jx_importcontroller/queryImportWastePaper`,
    "RangeWeight":  `${GROBAL_HTTP_CTX}/jx_importedPaper/rangeweight`,
    "ReasonList":  `${GROBAL_HTTP_CTX}/bdref/blobRefSearch?id=不合格原因`,
    "SAVE_ORDER":  `${GROBAL_HTTP_CTX}/jx_importedPaper/save`,
    "GET_LIST":  `${GROBAL_HTTP_CTX}/jx_importedPaper/list`,
    "DEL_ORDER":  `${GROBAL_HTTP_CTX}/jx_importedPaper/deleteBatch`,
    "Verify": `${GROBAL_HTTP_CTX}/jx_importedPaper/verify`,
    "CancelVerify": `${GROBAL_HTTP_CTX}/jx_importedPaper/cancelverify`,
    "GetAuth":`/wbalone/security/auth`,
    // 打印
    "GET_QUERYPRINTTEMPLATEALLOCATE":  `/eiap-plus/appResAllocate/queryPrintTemplateAllocate`,
    "PRINTSERVER": '/print_service/print/preview',

    "GET_TOEXPORTEXCEL":  `${GROBAL_HTTP_CTX}/file_up/download`,
}

/**
 * 获取列表
 * @param {*} params
 */
// export const getList = (params) => {
//     let url =URL.GET_LIST+'?1=1';
//     for(let attr in params.whereParams){
//         url+='&search_'+attr+'='+ params.whereParams[attr];
        
//     }
//     for(let attr1 in params.pageParams){
//         url+='&'+attr1+'='+ params.pageParams[attr1];
        
//     }
//     return request(url, {
//         method: "get",
//         data: params
//     });
// }

export const getList = (params) => {
    let url =URL.GET_LIST+'?1=1';
    let tempParam = {};
    for(let attr in params.whereParams){
        // tempParam[attr] = params.whereParams[attr];
        tempParam['search_'+attr] = params.whereParams[attr];
        
    }
    for(let attr1 in params.pageParams){
        tempParam[attr1] = params.pageParams[attr1];
        //url+='&'+attr1+'='+ params.pageParams[attr1];
    }
    return request(url, {
        method: "get",
        param: tempParam
    });
}


/**
 * 获取下拉列表
 * @param {*} params
 */
export const getSelect = (params) => {
    return request(URL.GET_SELECT, {
        method: "get",
        data: params
    });
}

// export const getRefHeapNo = (params) => {
//     return request(URL.refHeapNo, {
//         method: "get",
//         id: params
//     });
// }

// export const getRefrefWharf = (params) => {
//     return request(URL.refWharf, {
//         method: "get",
//         id: params
//     });
// }

export const getReasonList = () => {
    return request(URL.ReasonList, {
        method: "get"
    });
}

/**
 * 删除table数据
 * @param {*} params
 */
export const deleteList = (params) => {
    return request(URL.DELETE, {
        method: "post",
        data:params
    });
}

export const getAuth = (funcCode) => {
    return request(URL.GetAuth,{
        method:"get",
        param:{
            funcCode
        }
    });
}


/**
 * 删除table数据
 * @param {*} params
 */
export const Verify = (params) => {
    return request(URL.Verify, {
        method: "post",
        data:params
    });
}
/**
 * 删除table数据
 * @param {*} params
 */
export const CancelVerify = (params) => {
    return request(URL.CancelVerify, {
        method: "post",
        data:params
    });
}

/**
 * 获取下拉列表
 * @param {*} params
 */
export const OnRangeWeight = (params) => {
    return request(URL.RangeWeight, {
        method: "post",
        data: params
    });
}

export const saveList = (params) => {
    return request(URL.SAVE, {
        method: "post",
        data:params
    });
}
export const saveImportedPaper = (params) => {
    return request(URL.SAVE_ORDER, {
        method: "post",
        data: params
    });
}
export const delImportedPaper = (params) => {
    return request(URL.DEL_ORDER, {
        method: "post",
        data: params
    });
}

/**
 * 通过search_id 查询列表详情
*/

export const getDetail = (params) => {
    return request(URL.GET_DETAIL, {
        method: "get",
        param: params
    });
}
// 打印

export const queryPrintTemplateAllocate = (params) => {
    return request(URL.GET_QUERYPRINTTEMPLATEALLOCATE, {
        method: "get",
        param: params
    });    
    
}

export const printExcel = (params) => {

    let search = [];
    for(let key in params){
        search.push(`${key}=${params[key]}`)
    }
    let exportUrl = `${URL.PRINTSERVER}?${search.join('&')}`;
    console.log(exportUrl);
    window.open(exportUrl);     
    
}
/**
 * 导出
 */
export const exportExcel = (params) => {
    exportData(URL.GET_TOEXPORTEXCEL, params);
}

const selfURL = window[window.webkitURL ? 'webkitURL' : 'URL'];
let exportData = (url,data) => {
    axios({
        method : 'post',
        url : url,
        data : data,
        responseType : 'blob'
    }).then((res) => {
        const content = res.data;
        const blob = new Blob([content]);
        const fileName = "导出数据.xls";

        let elink = document.createElement('a');
        if('download' in elink) {
            elink.download = fileName;
            elink.style.display = 'none';
            elink.href = selfURL['createObjectURL'](blob);
            document.body.appendChild(elink);

            // 触发链接
            elink.click();
            selfURL.revokeObjectURL(elink.href);
            document.body.removeChild(elink)
        } else {
            navigator.msSaveBlob(blob, fileName);
        }
    })
}
