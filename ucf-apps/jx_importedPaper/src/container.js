import React from 'react';
import mirror, { connect } from 'mirrorx';

// 组件引入
import ImportedPaperTable from './components/importedPaper-root/ImportedPaperTable';
import ImportedPaperSelectTable from './components/importedPaper-root/ImportedPaperSelectTable';
import ImportedPaperPaginationTable from './components/importedPaper-root/ImportedPaperPaginationTable';
import ImportedPaperEdit from './components/importedPaper-edit/Edit';
import ImportedPaperBpmChart from './components/importedPaper-bpm-chart'
// 数据模型引入
import model from './model'
mirror.model(model);

// 数据和组件UI关联、绑定
export const ConnectedImportedPaperTable = connect( state => state.importedPaper, null )(ImportedPaperTable);
export const ConnectedImportedPaperSelectTable = connect( state => state.importedPaper, null )(ImportedPaperSelectTable);
export const ConnectedImportedPaperPaginationTable = connect( state => state.importedPaper, null )(ImportedPaperPaginationTable);
export const ConnectedImportedPaperEdit = connect( state => state.importedPaper, null )(ImportedPaperEdit);
export const ConnectedImportedPaperBpmChart = connect( state => state.importedPaper, null )(ImportedPaperBpmChart);
