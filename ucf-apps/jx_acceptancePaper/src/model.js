import { actions } from "mirrorx";
// 引入services，如不需要接口请求可不写
import * as api from "./service";
// 接口返回数据公共处理方法，根据具体需要
import { processData } from "utils";
import moment from 'moment';

/**
 *          btnFlag为按钮状态，新增、修改是可编辑，查看详情不可编辑，
 *          新增表格为空
 *          修改需要将行数据带上并显示在卡片页面
 *          查看详情携带行数据但是表格不可编辑
 *          0表示新增、1表示编辑，2表示查看详情 3提交
 *async loadList(param, getState) {
 *          rowData为行数据
*/


export default {
    // 确定 Store 中的数据模型作用域
    name: "acceptancePaper",
    // 设置当前 Model 所需的初始化 state
    initialState: {
        rowData:{},
        showLoading:false,
        list: [],
        childList: [],
        orderTypes:[],
        pageIndex:1,
        pageSize:10,
        totalPages:1,
        queryParam: {
            pageParams: {
                pageIndex: 0,
                pageSize: 10,
            },
            whereParams: {}
        },
        total:0,
        detail:{},
        searchParam:{},
        validateNum:99,//不存在的step

    },
    reducers: {
        /**
         * 纯函数，相当于 Redux 中的 Reducer，只负责对数据的更新。
         * @param {*} state
         * @param {*} data
         */
        updateState(state, data) { //更新state
            return {
                ...state,
                ...data
            };
        }
    },
    effects: {
        /**
         * 加载列表数据
         * @param {*} param
         * @param {*} getState
         */
        async loadList(param, getState) {
            // 正在加载数据，显示加载 Loading 图标
            actions.acceptancePaper.updateState({showLoading: true});
            // 调用 getList 请求数据
            let _param = param || getState().acceptancePaper.queryParam;
            let res = processData(await api.getList(param));
            //let {data:res}=result;
            let _state = {
                showLoading: false,
                queryParam: _param //更新搜索条件
            }
            if (res) {
                _state = Object.assign({}, _state, {
                    list: res.content,
                    pageIndex: res.number + 1,
                    totalPages: res.totalPages,
                    total: res.totalElements,
                    pageSize: res.size,
                })
            }
            actions.acceptancePaper.updateState(_state);
        },

        async loadChildList(param, getState) {
            // 正在加载数据，显示加载 Loading 图标
            actions.acceptancePaper.updateState({ showLoading:true })
            
            // 调用 getList 请求数据
            let res = processData(await api.getChildList(param));
            actions.acceptancePaper.updateState({  showLoading:false })
            debugger;
            if (res) {
                if(res.content&&res.content.length){
                    for(let i=0;i<res.content.length;i++){
                        let temp = Object.assign({},res.content[i]);
                        res.content[i].key=i+1;
                    }
                }
                // console.log('res content',res.content);
                actions.acceptancePaper.updateState({
                    childList: res.content,
                });
            }
        },

        

        /**
         * getSelect：获取下拉列表数据
         * @param {*} param
         * @param {*} getState
         */
        getOrderTypes(param,getState){
            actions.acceptancePaper.updateState({
            orderTypes:  [{
                "code":"0",
                "name":"D001"
            },{
                "code":"1",
                "name":"D002"
            },{
                "code":"2",
                "name":"D003"
            },{
                "code":"3",
                "name":"D004"
            }]
            })
        },

        /**
         * getSelect：保存table数据
         * @param {*} param
         * @param {*} getState
         */
        async saveList(param, getState) {
            let result = await api.saveList(param);
            return result;
        },
        /**
         * 删除table数据
         * @param {*} id
         * @param {*} getState
         */
        async removeList(id, getState) {
            let result = await api.deleteList([{id}]);
            return result;
        },

        async confirmVerifyItem(param,getState){
            actions.acceptancePaper.updateState({
              showLoading:true
            })
            let res=processData(await api.confirmVerifyItem(param),'操作成功');
        },

        async save(param,getState){//保存
            actions.acceptancePaper.updateState({
              showLoading:true
            })
            let res = processData(await api.saveAcceptancePaper(param),'保存成功');
            console.log("保存信息",res);
            if(res){
               window.history.go(-1);
            }
            actions.acceptancePaper.updateState({
                showLoading:false,

            });
        },

        async queryDetail(param,getState) {
            let {data:{detailMsg:{data:{content}}}}=await api.getDetail(param);
            return content[0];
        },

        async printExcel(param) {
            let res=processData(await api.queryPrintTemplateAllocate(param.queryParams),'');
            if(!res || !res.res_code) return false;
            await api.printExcel({
                tenantId: 'tenant',
                printcode: res.res_code,
                serverUrl: `${GROBAL_HTTP_CTX}/jx_acceptancePaper/dataForPrint`,
                params: encodeURIComponent(JSON.stringify(param.printParams)),
                sendType: 'post'
            })
        },
        
        async exportExcel(param) {
            api.exportExcel(param || {});
        },


    }
};