import React from 'react';
import mirror, { connect } from 'mirrorx';

// 组件引入
import AcceptancePaperTable from './components/acceptancePaper-root/AcceptancePaperTable';
import AcceptancePaperSelectTable from './components/acceptancePaper-root/AcceptancePaperSelectTable';
import AcceptancePaperPaginationTable from './components/acceptancePaper-root/AcceptancePaperPaginationTable';
import AcceptancePaperEdit from './components/acceptancePaper-edit/Edit';
import AcceptanceChildPaperEdit from './components/acceptanceChildPaper-edit';
import AcceptancePaperBpmChart from './components/acceptancePaper-bpm-chart'
// 数据模型引入
import model from './model'
mirror.model(model);

// 数据和组件UI关联、绑定
export const ConnectedAcceptancePaperTable = connect( state => state.acceptancePaper, null )(AcceptancePaperTable);
export const ConnectedAcceptancePaperSelectTable = connect( state => state.acceptancePaper, null )(AcceptancePaperSelectTable);
export const ConnectedAcceptancePaperPaginationTable = connect( state => state.acceptancePaper, null )(AcceptancePaperPaginationTable);
export const ConnectedAcceptancePaperEdit = connect( state => state.acceptancePaper, null )(AcceptancePaperEdit);
export const ConnectedAcceptanceChildPaperEdit = connect( state => state.acceptanceChildPaper, null )(AcceptanceChildPaperEdit);
export const ConnectedAcceptancePaperBpmChart = connect( state => state.acceptancePaper, null )(AcceptancePaperBpmChart);
