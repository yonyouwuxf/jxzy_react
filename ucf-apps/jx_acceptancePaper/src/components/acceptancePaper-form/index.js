import React, { Component } from 'react'
import { actions } from "mirrorx";
import { Switch, InputNumber, Col, Row,FormControl, Label, Select,Form,Radio } from "tinper-bee";
import SearchPanel from 'components/SearchPanel';
import DatePicker from "tinper-bee/lib/Datepicker";
// import RefMultipleTableWithInput from 'pap-refer/lib/ref-multiple-table/src/index';
// import 'pap-refer/lib/ref-multiple-table/src/index.css'; 
import {RefMultipleTableWithInput} from 'pap-refer/dist/index';
 import 'pap-refer/dist/index.css';
import { deepClone } from "utils";
import moment from 'moment';
const { RangePicker } = DatePicker;
const FormItem = Form.FormItem;
import './index.less'

class AcceptancePaperForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            pk_acceptancePaper: '',
        }
    }
    componentWillMount(){
        // 获得国内废纸验收列表数据
        actions.acceptancePaper.getOrderTypes();
    }
    /** 查询数据
     * @param {*} error 校验是否成功
     * @param {*} values 表单数据
     */
    search = (error,values) => {
        const {searchValuesCall} = this.props;
        this.props.form.validateFields(async (err, values) => {

            if(values.billDate[0]) {
                values.startBillDate = values.billDate[0] && values.billDate[0].format("YYYY-MM-DD") + ' 00:00:00';
                values.endBillDate = values.billDate[1] && values.billDate[1].format("YYYY-MM-DD") + ' 23:59:59';
            }

            if(values.materialVarietyCode && JSON.parse(values.materialVarietyCode).refpk){
                values.pk_material = JSON.parse(values.materialVarietyCode).refpk;
            }
            if(values.checkPoint && JSON.parse(values.checkPoint).refpk){
                values.checkPointpk=JSON.parse(values.checkPoint).refpk;
            }


            delete values.checkPoint;
            delete values.materialVarietyCode;
            delete values.billDate;

            for(let attr in values){
                if(!values[attr])
                    delete values[attr];
            }
            this.getQuery(values, 0);
        });


    }
    getQuery = (values, type) => {
        let queryParam = deepClone(this.props.queryParam);
        queryParam.whereParams = values;
        queryParam.pageParams = {
            pageIndex: 0,
            pageSize: 10,
        };
        actions.acceptancePaper.updateState(queryParam);
        if (type === 0) {
            actions.acceptancePaper.loadList(queryParam);
        }

    }

    initValueString = (name,pk) => {
        let param = {
            refname:name,
            refpk:pk
        }

        return JSON.stringify(param);
    }
    /**
     * 重置
     */
    reset = () => {
        let {queryParam} = this.props;
        queryParam.whereParams = {
            poundNo:"",
            qualityGrade:"",
            checkPoint:"",
            serialNo:"",
            materialVariety:"",
            pk_material:"",
            startBillDate:"",
            endBillDate:"",
            checkPointname:"",
            checkPointpk:"",
        };
        actions.acceptancePaper.updateState(queryParam);
    }
    render(){
        const { getFieldProps, getFieldError } = this.props.form;
        let { orderTypes,queryParam } = this.props;
        let self = this;
        let {
            poundNo,
            qualityGrade,
            checkPoint,
            serialNo,
            materialVariety="",
            pk_material="",
            startBillDate,
            endBillDate,
            checkPointname="",
            checkPointpk="",
        } = queryParam.whereParams || {};
        return (
            <SearchPanel
                className='search-form'
                form={this.props.form}
                reset={this.reset}
                search={this.search}>
                    <div className='demo-body'>
                        <Form>
                            <Row>
                                 <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('poundNo', {
                                                    initialValue: poundNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>序列号</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('serialNo', {
                                                    initialValue: serialNo,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>物料品种</Label>
                                        <RefMultipleTableWithInput
                                            title={'物料'}

                                            param={{
                                                "id": "物料",
                                                "sysId":"type1"
                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/materialref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/materialref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('materialVarietyCode', {
                                                initialValue: self.initValueString(materialVariety,pk_material),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>检验点</Label>
                                        <RefMultipleTableWithInput
                                            title={'检验点'}

                                            param={{
                                                "id": "检验点"
                                            }}
                                            refModelUrl={{
                                                tableBodyUrl: '/jxzy/bdref/blobRefSearch',//表体请求
                                                refInfo: '/jxzy/bdref/getRefModelInfo',//表头请求
                                            }}
                                            matchUrl='/pap_basedoc/common-ref/matchPKRefJSON'
                                            filterUrl='/pap_basedoc/common-ref/filterRefJSON'
                                            multiple={true}
                                            searchable={true}
                                            checkStrictly= {true}
                                            strictMode = {true}
                                            
                                            miniSearch={false}
                                            displayField='{refname}'
                                            valueField='refid'

                                            {...getFieldProps('checkPoint', {
                                                initialValue: self.initValueString(checkPointname,checkPointpk),
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>磅单日期</Label>
                                        <RangePicker
                                            format="YYYY-MM-DD"
                                            // locale={locale}
                                            placeholder={'开始 ~ 结束'}
                                            showClear={true}
                                            {...getFieldProps('billDate', {
                                                initialValue: startBillDate && [moment(startBillDate),moment(endBillDate)] || [moment(),moment()],
                                                validateTrigger: 'onBlur'
                                            })}
                                        />
                                    </FormItem>
                                </Col>
                                {/* <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>质量等级</Label>
                                        <FormControl
                                                {
                                                ...getFieldProps('qualityGrade', {
                                                    initialValue: qualityGrade,
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col> */}
                                <Col md={4} xs={6}>
                                    <FormItem>
                                        <Label>质量等级</Label>
                                        <Select
                                        className="audits-select"
                                        {
                                                ...getFieldProps('qualityGrade', {
                                                initialValue: qualityGrade || '',
                                            })
                                        }
                                        >       
                                                <Option value=''>请选择</Option>
                                                <Option value="AJ挂面纸">AJ挂面纸</Option>
                                                <Option value="挂面纸">挂面纸</Option>
                                                <Option value="AJ黄板">AJ黄板</Option>
                                                <Option value="超市">超市</Option>
                                                <Option value="黄板">黄板</Option>
                                                <Option value="I类">I类</Option>
                                                <Option value="II类">II类</Option>
                                                <Option value="III类">III类</Option>
                                                <Option value="IIII类">IIII类</Option>
                                                <Option value="IIIII类">IIIII类</Option>
                                                <Option value="化纤管">化纤管</Option>
                                                <Option value="保密纸">保密纸</Option>
                                                <Option value="高白">高白</Option>
                                                <Option value="低白">低白</Option>
                                                <Option value="散片AJ黄板">散片AJ黄板</Option>
                                        </Select>
                                    </FormItem>
                                </Col>
                            </Row>
                        </Form>
                    </div>
            </SearchPanel>
        )
    }
}

export default Form.createForm()(AcceptancePaperForm)