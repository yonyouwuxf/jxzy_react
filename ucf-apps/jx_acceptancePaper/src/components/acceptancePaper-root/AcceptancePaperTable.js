import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { actions } from "mirrorx";

import Header from 'components/Header';
import AcceptancePaperTable from '../acceptancePaper-table';
import AcceptancePaperForm from '../acceptancePaper-form';

import './index.less';

/**
 * AcceptancePaperRoot Component
 */
class AcceptancePaperRoot  extends Component {
    constructor(props) {
        super(props);
        this.state = { }
    }
    /**
     *
     */
    componentWillMount() {
        this.getTableData();
    }
    /**
     * 获取table表格数据
     */
    getTableData = () => {
        actions.acceptancePaper.loadList();
    }

    render() {
        let { pageSize, pageIndex, totalPages} = this.props;
        return (
            <div className='acceptancePaper-root'>
                <Header title='国内废纸验收' back={true}/>
                <AcceptancePaperForm { ...this.props }/>
                <AcceptancePaperTable { ...this.props }/>
            </div>
        )
    }
}
export default AcceptancePaperRoot;